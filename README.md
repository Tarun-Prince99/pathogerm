# Pathogerm
Tarun Prince

    tp23539

    @Tarun-Prince99

    estimated completion time: 8

    actual completion time: 12


Michael Geng

    mg57492

    @mjgeng

    estimated completion time: 7

    actual completion time: 9


Paul Sathuluri

    pvs297

    @psathuluri

    estimated completion time: 8

    actual completion time: 11


Brian LeQuang

    bl25572

    @brainleq

    estimated completion time: 10

    actual completion time: 10


Darren Phua

    dwp663

    @dwcp

    estimated completion time: 8

    actual completion time: 9


project leader: Michael Geng

Git SHA: 9fe050d3a5cf0d885bd83f799ab2b9d7b7cf3812

link to GitLab pipelines: https://gitlab.com/Tarun-Prince99/pathogerm/pipelines

link to website: https://pathogerm.com/
