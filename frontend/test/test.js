import React from 'react';
import { configure, shallow, render} from 'enzyme';
import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
// import App from '../App';
import chaiEnzyme from 'chai-enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Landing from '../src/views/Landing';
import Map from '../src/views/Map';
import { Route, BrowserRouter } from 'react-router-dom';
import Diseases from '../src/views/Diseases';
import About from '../src/views/About';
import { StatsTable } from '../src/components/StatsTable';
import Timeline from '../src/views/Timeline';
import Information from '../src/views/Information';
import request from 'request';
import Navbar from '../src/components/Navbar';
import ReactDOM from 'react-dom';
import Search from '../src/views/Search';
import MapChart from '../src/components/MapChart';
import { DataChart } from '../src/components/DataChart';
var App = '../src/App.js'
// var expect = require('chai').expect;
// var req = require('request')
var baseUrl = "http://localhost:3000/";


configure({ adapter: new Adapter() });

describe('Components Render', function() {

  it('App', function () {
      const wrapper = shallow(<App />); 
      expect(wrapper).to.exist
  });
  it('Landing', function () {
    const wrapper = shallow(<Landing />); 
    expect(wrapper).to.exist;
    expect(wrapper.find("h1")).to.not.be.undefined;
    expect(wrapper.find("h1").text()).to.equal("PΛTHOGERM");
  })
  it('Landing HTML Works', function () {
    const wrapper = shallow(<Landing />); 
    expect(wrapper.find("h1").text()).to.equal("PΛTHOGERM");
  })
  //has pararmaters or prop changes
  // it('Map', function () {
  //   const wrapper = shallow(<Map />); 
  //   expect(wrapper).to.exist
  // })
  it('Diseases', function () {
    const wrapper = shallow(<Diseases />); 
    expect(wrapper).to.exist;
    expect(wrapper.find("h1")).to.not.be.undefined;
    expect(wrapper.find("h1").text()).to.equal("DISEASES");
  })
  it('Diseases HTML works', function () {
    const wrapper = shallow(<Diseases />); 
    expect(wrapper.find("h1").text()).to.equal("DISEASES");
  })
  //print out list of info
  // it('Timeline', function () {
  //   const wrapper = shallow(<Timeline />); 
  //   expect(wrapper).to.exist
  // })
  it('StatsTable', function () {
    const wrapper = shallow(<StatsTable />); 
    expect(wrapper).to.exist
    expect(wrapper.find("StatsTable")).to.not.be.undefined;
  })
  //has pararmaters or prop changes
  // it('Information', function () {
  //   const wrapper = shallow(<Information />); 
  //   expect(wrapper).to.exist
  // })
  it('NavBar', function () {
    const wrapper = shallow(<Navbar/>); 
    expect(wrapper).to.exist
    expect(wrapper.find("nav-link")).to.not.be.undefined;
  })
  it('DataChart', function () {
    const wrapper = shallow(<DataChart/>); 
    expect(wrapper).to.exist
  })
  

    chai.use(chaiEnzyme())
  })




describe("Model-Pages-Respond", function() {

    it("Landing", function(done) {
        request(baseUrl, function(error, response, body) {
        expect(body).to.not.equal(null);
            done();
      });
    });

    it("Info", function(done) {
      request(baseUrl+"information/Acne%20vulgaris", function(error, response, body) {
      expect(body).to.not.equal(null);
          done();
    });
  });

    it("About", function(done) {
        request(baseUrl+"about", function(error, response, body) {
        //   expect(response.statusCode).to.equal(200);
        expect(body).to.not.equal(null);
            done();
        });
    });
    
    it("Timeline", function(done) {
      request(baseUrl+"timeline", function(error, response, body) {
        expect(body).to.not.equal(null);
            done();
      });
    });

    it("Diseases", function(done) {
        request(baseUrl+"diseases", function(error, response, body) {
        expect(body).to.not.equal(null);
            done();
        });
    });

    it("Map", function(done) {
        request(baseUrl+"map", function(error, response, body) {
            expect(body).to.not.equal(null);
            done();
        });
      });

      it("Landing", function(done) {
        request(baseUrl, function(error, response, body) {
          expect(body).to.not.equal(null);
              done();
        });
      });
  
});


