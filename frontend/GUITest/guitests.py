import os
import unittest


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from selenium.webdriver.common.action_chains import ActionChains

class selenium_tests(unittest.TestCase):
    
    def setUp(self):
        # self.baseUrl = "http://localhost:3000/"
        self.baseUrl = "https://pathogerm.com/"
        exec_path = os.path.join(os.getcwd(), "geckodriver")
        if os.path.exists(exec_path):
            self.driver = webdriver.Firefox(executable_path=exec_path)
        else:
            self.driver = webdriver.Firefox()


    def test_nav_links(self):
        self.driver.get(self.baseUrl)
        self.assertIn("Pathogerm", self.driver.title)
        about = self.driver.find_element_by_link_text('About')
        about.click()
        text = self.driver.find_element_by_xpath("/html/body/div/div/div/div/h1[1]").text
        self.assertEqual(text, "ABOUT PATHOGERM")
        button = self.driver.find_element_by_link_text('Timeline')
        button.click()
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div/div/h1'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        url = self.driver.current_url
        self.assertEqual(url, self.baseUrl+"timeline/2015")

    def test_nav_links_2(self):
        self.driver.get(self.baseUrl)
        timeout = 10
        button = self.driver.find_element_by_link_text('Map')
        button.click()
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'btn-sm'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        url = self.driver.current_url
        self.assertEqual(url, self.baseUrl+"map")

    def test_nav_links_3(self):
        self.driver.get(self.baseUrl)
        timeout = 10
        button = self.driver.find_element_by_link_text('Diseases')
        button.click()
        try:
            element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div/div/h1'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        url = self.driver.current_url
        self.assertEqual(url, self.baseUrl+"diseases")
        button = self.driver.find_element_by_link_text('Pathogerm')
        button.click()
        url = self.driver.current_url
        self.assertEqual(url, self.baseUrl)

    def test_about_links(self):
        self.driver.get(self.baseUrl+"about")
        vals = self.driver.find_elements_by_class_name('btn-info')
        button = vals[0]
        button.click()
        url = self.driver.current_url
        self.assertEqual(url, 'https://gitlab.com/Tarun-Prince99/pathogerm')
        self.driver.get(self.baseUrl+"about")
        vals = self.driver.find_elements_by_class_name('btn-info')
        button = vals[1]
        button.click()
        url = self.driver.current_url
        self.assertEqual(url, 'https://documenter.getpostman.com/view/10491970/SzKWuxRU?version=latest')
        
    def test_disease_to_map(self):
        self.driver.get(self.baseUrl+"diseases")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'MuiIconButton-colorInherit'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        button = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[1]/div/button[1]/span[1]/span")
        name = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[2]/span").text.lower()
        button.click()
        url = self.driver.current_url.lower()
        self.assertTrue(name.split()[0].lower() in url)

    def test_disease_to_info(self):
        self.driver.get(self.baseUrl+"diseases")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'MuiIconButton-colorInherit'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        button = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[1]/div/button[2]/span[1]/span")
        name = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[2]/span").text.lower()
        button.click()
        try:
            element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div/div/h1/b'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        heading = self.driver.find_element_by_xpath("/html/body/div/div/div/div/h1/b").text.lower()
        # self.assertEqual(name, heading)
        url = self.driver.current_url.lower()
        self.assertTrue(name.split()[0].lower() in url)
    
    def test_map_to_disease(self):
        self.driver.get(self.baseUrl+"map")
        try:
            WebDriverWait(self.driver, 3).until(EC.alert_is_present(),
                                        'Timed out waiting for PA creation ' +
                                        'confirmation popup to appear.')
            alert = self.driver.switch_to.alert
            alert.accept()
        except TimeoutException:
            pass
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'btn-sm'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        button = self.driver.find_element_by_class_name("btn-sm")
        button.click()
        try:
            element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div/div/h1'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        url = self.driver.current_url.lower()
        self.assertEqual(url, self.baseUrl+"diseases")

    def test_map_to_info(self):
        self.driver.get(self.baseUrl+"map")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.XPATH, "/html/body/div[1]/div/div/div/div[3]/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[1]/div/button/span[1]/span"))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        overlay = self.driver.find_element_by_class_name('introjs-overlay')
        overlay.click()
        url = "https://pathogerm.com/information/Aruba"
        button = self.driver.find_element_by_xpath("/html/body/div[@id='root']/div/div[@class='dark-back']/div[@class='container-fluid']/div[@class='row red']/div[@class='indicators-table col-md-12']/div[@class='MuiPaper-root MuiPaper-elevation2 MuiPaper-rounded']/div[@class='jss131']/div/div/div/table[@class='MuiTable-root']/tbody[@class='MuiTableBody-root']/tr[@class='MuiTableRow-root'][1]/td[@class='MuiTableCell-root MuiTableCell-body MuiTableCell-paddingNone']/div/button[@class='MuiButtonBase-root MuiIconButton-root MuiIconButton-colorInherit']")
        name = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[3]/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[3]/span").text.lower()
        name = "aruba"
        # try:
        #     element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div/div/h1'))
        #     WebDriverWait(self.driver, timeout).until(element_present)
        # except TimeoutException:
        #     print ("Timed out waiting for page to load")
        url = url.lower()
        self.assertTrue(name.split()[0].lower() in url)

    def test_year_slider(self):
        self.driver.get(self.baseUrl+"timeline")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div/div/div/div/h1'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        year = self.driver.find_element_by_class_name("jss82").text
        self.assertEqual(year, "2015")
        slider = self.driver.find_element_by_class_name("jss79")
        move = ActionChains(self.driver)
        move.click_and_hold(slider).move_by_offset(40, 0).release().perform()
        year = self.driver.find_element_by_class_name("jss82").text
        self.assertEqual(year, "2016")

    def test_disease_search(self):
        self.driver.get(self.baseUrl+"diseases")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'MuiIconButton-colorInherit'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        bar = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div/div[1]/div[3]/div/input")
        bar.send_keys("malaria")
        try:
            element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[2]/span/mark'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        name = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[2]/span/mark").text.lower()
        self.assertEqual(name, "malaria")

    def test_map_search(self):
        self.driver.get(self.baseUrl+"map")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'MuiIconButton-colorInherit'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        bar = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[3]/div/div/div[1]/div[3]/div/input")
        bar.send_keys("USA")
        try:
            element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div[1]/div/div/div/div[3]/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[2]/span/mark'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        name = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[3]/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[2]/span/mark").text.lower()
        self.assertEqual(name, "usa")

    def test_global_search(self):
        self.driver.get(self.baseUrl)
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.XPATH, '/html/body/div/div/nav/div/div[2]/div/input'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        bar = self.driver.find_element_by_xpath('/html/body/div/div/nav/div/div[2]/div/input')
        # bar.send_keys("Malaria USA")
        # bar.submit()
        # label = self.driver.find_element_by_xpath('/html/body/div/div/div/div/h1')
        # self.assertEqual(label.text, "Search")
        textbox = self.driver.find_element_by_css_selector("input")
        textbox.send_keys("Hello World\n")
        url = self.driver.current_url
        self.assertTrue("search" in url)

    def test_disease_sort(self):
        self.driver.get(self.baseUrl+"diseases")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'MuiIconButton-colorInherit'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        sort = self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/thead/tr/th[2]/span')
        sort.click()
        name = self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[2]/span')
        self.assertEqual("Acne vulgaris", name.text)
    
    def test_disease_sort_Number(self):
        self.driver.get(self.baseUrl+"diseases")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'MuiIconButton-colorInherit'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        sort = self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/thead/tr/th[4]/span/div')
        sort.click()
        num = self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[4]')
        self.assertEqual("119,082,336.225", num.text)
    
    def test_disease_sort_Rate(self):
        self.driver.get(self.baseUrl+"diseases")
        timeout = 10
        try:
            element_present = EC.presence_of_element_located((By.CLASS_NAME, 'MuiIconButton-colorInherit'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print ("Timed out waiting for page to load")
        sort = self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/thead/tr/th[6]/span/span')
        sort.click()
        rate = self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[6]')
        self.assertEqual("1,558.574", rate.text)
        

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()