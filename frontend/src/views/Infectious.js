import React from 'react';
import { Card } from 'react-bootstrap';

import '../css/Text.css';
import '../css/Page.css';

export default class Infectious extends React.Component {
  render() {
    return (
      <div className="containter dark-back" style={{position:'relative'}}>
        <div className="container formPad raleWay">
          <h1 id="letter-spacing" className="red" style={{paddingBottom: '35px'}}>INFECTIOUS</h1>

            <div className="container">
            
              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>AIDS</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Virus</Card.Subtitle>
                    <Card.Text>
                    <p>Mortality Rate: 1.7 / 100 person-years</p>
                    <p>Survival Rate: 87%</p>
                    <p>Transmission: Bodily fluids</p>
                    <p>Treatable: False</p>
                    <p>Location of Origin: Africa</p>
                      💀
                    </Card.Text>
                    <Card.Link className="red" href="/in1">More</Card.Link>
                  </Card.Body>
                </Card>
              </button>
              
              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>Malaria</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Protozoan parasites</Card.Subtitle>
                    <Card.Text>
                    <p>Mortality Rate: 1-3 million</p>
                    <p>Survival Rate: 90%</p>
                    <p>Transmission: Mosquito bite</p>
                    <p>Treatable: True</p>
                    <p>Location of Origin: Africa</p>
                      💀
                    </Card.Text>
                    <Card.Link className="red" href="/in2">More</Card.Link>
                  </Card.Body>
                </Card>
              </button>

              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>Bubonic Plague</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Yersinia pestis</Card.Subtitle>
                    <Card.Text>
                    <p>Mortality Rate: 66 out of 100</p>
                    <p>Survival Rate: 85%</p>
                    <p>Transmission: Infectious Droplets, Flea Bites</p>
                    <p>Treatable: True</p>
                    <p>Location of Origin: China</p>
                      💀
                    </Card.Text>
                    <Card.Link className="red" href="/in3">More</Card.Link>
                  </Card.Body>
                </Card>
              </button>

              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>Chicken Pox</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Varicella zoster</Card.Subtitle>
                    <Card.Text>
                    <p>Mortality Rate: 103</p>
                    <p>Survival Rate: 95%</p>
                    <p>Transmission: Person to person contact</p>
                    <p>Treatable: True</p>
                    <p>Location of Origin: Japan</p>
                      💀
                    </Card.Text>
                    <Card.Link className="red" href="/in4">More</Card.Link>
                  </Card.Body>
                </Card>
              </button>

              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>Tuberculosis</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Bacteria</Card.Subtitle>
                    <Card.Text>
                    <p>Mortality Rate: 3 out of 100 people per year</p>
                    <p>Survival Rate: 12.3%</p>
                    <p>Transmission: Air</p>
                    <p>Treatable: True</p>
                    <p>Location of Origin: Egypt</p>
                      💀
                    </Card.Text>
                    <Card.Link className="red" href="/in5">More</Card.Link>
                  </Card.Body>
                </Card>
              </button>

              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>Influenza</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Virus</Card.Subtitle>
                    <Card.Text>
                    <p>Mortality Rate: 12000-60000 deaths total</p>
                    <p>Survival Rate: 0.1 to 20%</p>
                    <p>Transmission: Cough/Sneeze</p>
                    <p>Treatable: True</p>
                    <p>Location of Origin: Asia</p>
                      💀
                    </Card.Text>
                    <Card.Link className="red" href="/in6">More</Card.Link>
                  </Card.Body>
                </Card>
              </button>

              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>E. Coli</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Proteobacteria</Card.Subtitle>
                    <Card.Text>
                    <p>Mortality Rate: less than 1%</p>
                    <p>Survival Rate: 99%</p>
                    <p>Transmission: Contamination</p>
                    <p>Treatable: True</p>
                    <p>Location of Origin: Africa</p>
                      💀
                    </Card.Text>
                    <Card.Link className="red" href="/in7">More</Card.Link>
                  </Card.Body>
                </Card>
              </button>

              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>Bacterial Meningitis</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Bacteria</Card.Subtitle>
                    <Card.Text>
                    <p>Mortality Rate: 30%</p>
                    <p>Survival Rate: 21%</p>
                    <p>Transmission: Cough/Sneeze</p>
                    <p>Treatable: True</p>
                    <p>Location of Origin: Switzerland</p>
                      💀
                    </Card.Text>
                    <Card.Link className="red" href="/in8">More</Card.Link>
                  </Card.Body>
                </Card>
              </button>

              <button className="marginSpace">
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>Title</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Subtitle</Card.Subtitle>
                    <Card.Text>
                      XXX
                    </Card.Text>
                    {/* <Card.Link className="red" href="/">More</Card.Link> */}
                  </Card.Body>
                </Card>
              </button>
            </div>

        </div>
      </div>
    )
  }
}
