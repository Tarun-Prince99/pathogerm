import React from 'react';
import { Card } from 'react-bootstrap';

import '../css/Text.css';
import '../css/Page.css';

export default class NonCommunicable extends React.Component {
  render() {
    return (
      <div className="containter dark-back" style={{position:'relative'}}>
        <div className="container formPad raleWay">
          <h1 id="letter-spacing" className="red" style={{paddingBottom: '35px'}}>NON-COMMUNICABLE</h1>
          <div className="container">
            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Type I Diabetes</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Deficient Insulin Production</Card.Subtitle>
                  <Card.Text>
                  <p>Mortality Rate: 10%</p>
                  <p>5-Year Survival Rate: 90%</p>
                  <p>Body Region Affected: Pancreas</p>
                  <p>Treatable: True</p>
                  <p>Average Age at Onset: 14 Years</p>
                  </Card.Text>
                  <Card.Link className="red" href="/ncin2">More</Card.Link>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Asthma</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Chronic Lung Condition</Card.Subtitle>
                  <Card.Text>
                  <p>Mortality Rate: less than 1%</p>
                  <p>5-Year Survival Rate: 99%</p>
                  <p>Body Region Affected: Respiratory System</p>
                  <p>Treatable: True</p>
                  <p>Average Age at Onset: 3 Years</p>
                  </Card.Text>
                  <Card.Link className="red" href="/ncin1">More</Card.Link>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Ischemic Stroke</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Cerebrovascular Accident</Card.Subtitle>
                  <Card.Text>
                  <p>Mortality Rate: 20%</p>
                <p>5-Year Survival Rate: 31%</p>
                <p>Body Region Affected: Arteries, Brain</p>
                <p>Treatable: True</p>
                <p>Average Age at Onset: 65 Years</p>
                  </Card.Text>
                  <Card.Link className="red" href="/ncin3">More</Card.Link>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Title</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Subtitle</Card.Subtitle>
                  <Card.Text>
                    XXX
                  </Card.Text>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Title</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Subtitle</Card.Subtitle>
                  <Card.Text>
                    XXX
                  </Card.Text>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Title</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Subtitle</Card.Subtitle>
                  <Card.Text>
                    XXX
                  </Card.Text>
                </Card.Body>
              </Card>
            </button>
          </div>
        </div>
      </div>
    )
  }
}
