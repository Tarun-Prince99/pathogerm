import React from "react";
import axios from "axios"
import API_PATH from "../components/ApiPath.js";
import Highlighter from "react-highlighter"
import IconButton from '@material-ui/core/IconButton';
import MapIcon from '@material-ui/icons/Map';
import InfoIcon from '@material-ui/icons/Info';
import Tooltip from '@material-ui/core/Tooltip';

import '../css/Page.css';
import '../css/Text.css';

export default class Search extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      query: this.props.match.params.id,
      disease_model: null,
      country_model: null,
      year_model: null,
      d_load: 0,
      c_load: 0,
      y_load: 0
    }
    this.updateData()
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.setState({
        query: this.props.match.params.id,
        disease_model: null,
        country_model: null,
        year_model: null,
        d_load: 0,
        c_load: 0,
        y_load: 0
      }, this.updateData)
    }
  }

  updateData() {
    axios({
      method: "get",
      url: API_PATH + "diseases/?limit=350&measure=Prevalence&year=2017&location=GLO&search=" + this.state.query
    }).then((response) => {
      this.setState({ disease_model: response.data.results, d_load: 1 })
    }).catch((error) => {
      this.setState({ d_load: -1 })
    })

    axios({
      method: "get",
      url: API_PATH + "countries/?limit=350&search=" + this.state.query
    }).then((response) => {
      this.setState({ country_model: response.data.results, c_load: 1 })
    }).catch((error) => {
      this.setState({ c_load: -1 })
    })

    axios({
      method: "get",
      url: API_PATH + "years/?limit=350&search=" + this.state.query
    }).then((response) => {
      this.setState({ year_model: response.data.results, y_load: 1 })
    }).catch((error) => {
      this.setState({ y_load: -1 })
    })
  }


  render() {
    if(this.state.d_load === 0 || this.state.c_load === 0 || this.state.y_load === 0)
      return <div className="content-center"><h1 id="letter-spacing">LOADING...</h1></div>
    if(this.state.d_load === -1 || this.state.c_load === -1 || this.state.y_load === -1)
      return <div className="content-center"><h1 id="letter-spacing">FAILED TO LOAD</h1></div>

    function filterWords(word, name){
        let queryList = word.split(" ");
        var master_string = "";
        for(let i = 0; i < queryList.length; i++){
          if(!Number.isInteger(name))
            name = name.toLowerCase();
          if(name.toString().includes(queryList[i].toLowerCase())){
            master_string += (queryList[i] + " ");
          }
        }
        master_string = master_string.trim();
        return master_string;
    }

    var c = 0
    const disease_results = this.state.disease_model.map(result => {
      c += 1
      return (
        <div key={c}>
          <Tooltip title="View Map">
            <IconButton
              onClick={() => {this.props.history.push("/map/" + result.cause)}}
              className="pull-left"
              aria-label="View Map"
              size="small"
            >
              <MapIcon style={{color: "white"}}/>
            </IconButton>
          </Tooltip>
          <Tooltip title="More Information">
            <IconButton
              onClick={() => {this.props.history.push("/information/" + result.cause)}}
              className="pull-left"
              aria-label="More Information"
              size="small"
            >
              <InfoIcon style={{color: "white"}}/>
            </IconButton>
          </Tooltip>
          <span>
            <Highlighter search={filterWords(this.state.query, result.cause)}>{result.cause}</Highlighter>
          </span>
        </div>
      )
    })
    const country_results = this.state.country_model.map(result => {
      c += 1
      return (
        <div key={c}>
          <Tooltip title="More Information">
            <IconButton
              onClick={() => {this.props.history.push("/information/" + result.name)}}
              className="pull-left"
              aria-label="More Information"
              size="small"
            >
              <InfoIcon style={{color: "white"}}/>
            </IconButton>
          </Tooltip>
          <span>
            <Highlighter search={filterWords(this.state.query, result.name)}>{result.name}</Highlighter>
            <Highlighter search={filterWords(this.state.query, result.iso)}>{" (" + result.iso + ")"}</Highlighter>
          </span>
        </div>
      )
    })
    const year_results = this.state.year_model.map(result => {
      c += 1
      return (
        <div key={c}>
          <Tooltip title="More Information">
            <IconButton
              onClick={() => {this.props.history.push("/timeline/" + result.year)}}
              className="pull-left"
              aria-label="More Information"
              size="small"
            >
              <InfoIcon style={{color: "white"}}/>
            </IconButton>
          </Tooltip>
          <span>
            <Highlighter search={filterWords(this.state.query, result.year)}>{result.year.toString()}</Highlighter>
            <Highlighter search={filterWords(this.state.query, result.name)}>{" (" + result.name + ")"}</Highlighter>
          </span>
        </div>
      )
    })

    return (
      <div className="dark-back">
        <div className="container formPad raleWay">
          <h1 id="letter-spacing" className="red">SEARCH RESULTS</h1>
          <div className="white left">
            <h5>Showing results for: {this.state.query}</h5>
            <div className="row">
              <div className="col-md-5">
                <h3 id="letter-spacing" className="red" style={{paddingTop: "20px", paddingBottom: "10px"}}>DISEASES:</h3>
                {disease_results.length > 0 ? disease_results : <p className="red">Your search did not match any diseases.</p>}
              </div>
              <div className="col-md-5">
                <h3 id="letter-spacing" className="red" style={{paddingTop: "20px", paddingBottom: "10px"}}>COUNTRIES:</h3>
                {country_results.length > 0 ? country_results : <p className="red">Your search did not match any countries.</p>}
              </div>
              <div className="col-md-2">
                <h3 id="letter-spacing" className="red" style={{paddingTop: "20px", paddingBottom: "10px"}}>YEARS:</h3>
                {year_results.length > 0 ? year_results : <p className="red">Your search did not match any years.</p>}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}