import React from 'react';
import ReactTooltip from "react-tooltip";
import MapChart from "../components/MapChart.js";
import { StatsTable, StaticTable } from "../components/StatsTable.js";
import { Steps, Hints } from "intro.js-react";
import API_PATH from "../components/ApiPath.js";
import axios from "axios"

import 'intro.js/introjs.css';
import 'intro.js/themes/introjs-modern.css';
import "../css/Page.css"

export default class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "GLOBAL",
      iso: "GLO",
      tooltip: "",
      disease_model: null,
      country_model: null,
      all_disease_stats: null,
      d_load: 0,
      c_load: 0,
      steps_enabled: true,
      initial_step: 0,
      id: this.props.match.params.id ? this.props.match.params.id : "All causes"
    }
    this.updateData()
  }

  updateData() {
    if (this.state.iso === "GLO") {
      axios({
        method: "get",
        url: API_PATH + "diseases/?cause=" + this.state.id + "&limit=200&measure=Prevalence&year=2017"
      }).then((response) => {
        this.setState({ all_disease_stats: response.data })
      }).catch((error) => {
        alert("all_disease_stats error")
      })
    }

    axios({
      method: "get",
      url: API_PATH + "diseases/?cause=" + this.state.id + "&year=2017&location=" + this.state.iso
    }).then((response) => {
      this.setState({ disease_model: response.data, d_load: 1 })
    }).catch((error) => {
      this.setState({ d_load: -1 })
    })

    var temp_iso = this.state.iso === "GLO" ? "" : this.state.iso
    axios({
      method: "get",
      url: API_PATH + "countries/?iso=" + temp_iso
    }).then((response) => {
      this.setState({ country_model: response.data, c_load: 1 })
    }).catch((error) => {
      this.setState({ c_load: -1 })
    })
  }

  setContent = (tooltip) => {
    this.setState({tooltip: tooltip})
  }

  setCountry = (title) => {
    this.setState({title: title.toUpperCase()})
  }

  setISO = (iso) => {
    this.setState({iso: iso.toUpperCase()}, this.updateData)
  }

  onExit = () => {
    this.setState(() => ({ steps_enabled: false }));
  };

  formatContent(tooltip) {
    if (tooltip !== "")
      return tooltip.split('\n').map( (it, i) => <div key={'x'+i}>{it}</div> )
    else
      return tooltip
  }

  updateDiseaseStats() {

    function beautifyNumber(number, decimalCount, format) {
      let result = parseFloat(number).toFixed(decimalCount);
      let formattedResult = (format) ? result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        : (result)
      return formattedResult
    }

    var stats = []
    if (this.state.d_load === 1) {
      const model = this.state.disease_model.results
      for(var i = 0; i < model.length; ++i) {
        stats = stats.concat([
          { id: "", val: model[i].measure },
          { id: "Number", val: beautifyNumber(model[i].number, 3, true)},
          { id: "Percent", val: beautifyNumber(model[i].percent, 6, false)},
          { id: "Rate", val: beautifyNumber(model[i].rate, 3, true) }
        ])
      }
    }
    return stats
  }

  updateCountryStats() {
    var stats = []
    if (this.state.c_load === 1) {
      const model = this.state.country_model.results
      for(var i = 0; i < model.length; ++i) {
        stats = stats.concat([
          { iso: model[i].iso, name: model[i].name,
            pop: model[i].pop, gdp: model[i].gdp,
            mle: model[i].mle, fle: model[i].fle,
            cbr: model[i].cbr, imr: model[i].imr,
            com: model[i].com, noncom: model[i].noncom,
            md: model[i].md, inf_d: model[i].inf_d,
            hexp: model[i].hexp, incomelevel: model[i].incomelevel}
        ])
      }
    }
    return stats
  }

  selectCountryTable(country_title, country_header, country_stats) {
    if (this.state.iso === "GLO")
      return StatsTable(country_title, country_header, 10, "countries/?", false, ["name", "iso", "incomelevel"], 250)
    else
      return StaticTable(country_title, country_header, country_stats, 10, [5, 10, 15], true)
  }

  render() {

    function fixBadCase(number) {
      if(number===(-999999))
        return ""
    }

    function beautifyNumber(number) {
      let result = parseFloat(number).toFixed(3);
      return result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    // var disease_path = "diseases/?cause=" + this.state.id + "&year=2017&location=" + this.state.iso
    const disease_title = this.state.id
    const country_title = "Country Data"
    const disease_header = [
      {title: "Indicator", field: "id"},
      {title: "", field: "val"}
    ]
    const country_header = [
      {title: "ISO", field: "iso", customSort: (a, b) => a.iso.props.children.localeCompare(b.iso.props.children)},
      {title: "Name", field: "name", customSort: (a, b) => a.name.props.children.localeCompare(b.name.props.children)},
      {title: "Population", field: "pop", type: "numeric", render: rowData => rowData.pop ? beautifyNumber(rowData.pop) : null},
      {title: "GDP", field: "gdp", type: "numeric", render: rowData => rowData.gdp ? beautifyNumber(rowData.gdp) : null},
      {title: "Male Life Expectancy", field: "mle", type: "numeric"},
      {title: "Female Life Expectancy", field: "fle", type: "numeric"},
      {title: "Crude Birth Rate", field: "cbr", type: "numeric"},
      {title: "Infant Mortality Rate", field: "imr", type: "numeric"},
      {title: "Communicable Disease Prevalence", field: "com", type: "numeric"},
      {title: "Noncommunicable Disease Prevalence", field: "noncom", type: "numeric"},
      {title: "Physicians per 10,000", field: "md", type: "numeric", render: rowData => fixBadCase(rowData.md)},
      {title: "% Deaths from Communicable Disease", field: "inf_d", type: "numeric", render: rowData => fixBadCase(rowData.inf_d)},
      {title: "Health Expenditure", field: "hexp", type: "numeric", render: rowData => fixBadCase(rowData.hexp)},
      {title: "Income Level", field: "incomelevel"}
    ]
    var disease_stats = this.updateDiseaseStats()
    var country_stats = this.updateCountryStats()
    // const steps = [
    //   {
    //     element: ".disease-btn",
    //     intro: "Button to search for a new disease."
    //   },
    //   {
    //     element: ".map",
    //     intro: "Select a country to update the tables.",
    //     position: "left",
    //   },
    //   {
    //     element: ".disease-table",
    //     intro: "Info on the presence of the disease at this location.",
    //     position: "right",
    //   },
    //   {
    //     element: ".indicators-table",
    //     intro: "Basic indicators for this location.",
    //   }
    // ]

    return (
      <div className="dark-back">
        {/*<Steps
          enabled={this.state.steps_enabled}
          steps={steps}
          initialStep={this.state.initial_step}
          onExit={this.onExit}
        />*/}
        <div className="container-fluid">
          <div className="row" style={{paddingLeft:'30px', paddingRight:'30px'}}>
            <div className="col-md-12 red topic-banner raleWay">
              <h2 id="letter-spacing" style={{paddingBottom: "15px"}}> {(this.state.id).toUpperCase().replace(/_/g, " ")} </h2>
              <form action="/diseases">
                <button type="submit" className="disease-btn btn btn-outline-danger btn-sm">Search Another Disease</button>
              </form>
            </div>
          </div>
          <div className="row" style={{padding:'30px'}}>
            <div className="disease-table col-sm-12 col-md-4 outline red" style={{paddingLeft:"30px", paddingRight:"30px"}}>
              <h3 className="raleWay topic-banner" id="letter-spacing"> { this.state.title } </h3>
              {StaticTable(disease_title, disease_header, disease_stats, 12, [4, 8, 12], false)}
            </div>
            <div className="map col-sm-12 col-md-8 outline" style={{paddingLeft:'5px', paddingRight:'5px'}}>
              {<MapChart setTooltipContent={this.setContent} setCountry={this.setCountry} setISO={this.setISO} data={this.state.all_disease_stats}/>}
              <ReactTooltip multiline={true}>{this.formatContent(this.state.tooltip)}</ReactTooltip>
            </div>
          </div>
          <div className="row red" style={{paddingLeft: "30px", paddingRight: "30px", paddingBottom: "30px"}}>
            <div className="indicators-table col-md-12">
              <h1 className="raleWay topic-banner" id="letter-spacing"> { this.state.title } </h1>
              {this.selectCountryTable(country_title, country_header, country_stats)}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
