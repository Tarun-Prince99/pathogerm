import React from 'react';
import { Card } from 'react-bootstrap';

import '../css/Text.css';
import '../css/Page.css';

export default class Year extends React.Component {
  render() {
    return (
      <div className="containter dark-back" style={{position:'relative'}}>
        <div className="container formPad raleWay">
          <h1 id="letter-spacing" className="red" style={{paddingBottom: '35px'}}>YEAR</h1>
          <div className="container">
            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>2002</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">West Nile</Card.Subtitle>
                  <Card.Text>
                  <p>Highest Mortality Rate: 845 / 100,000 person-years</p>
                  <p>Birth Rate: 14 per 1000</p>
                  <p>Most Infectious Disease: West Nile</p>
                  <p>Deadliest Disease: Malaria</p>
                  <p>Highest Infected Country: Africa</p>
                  </Card.Text>
                  <Card.Link className="red" href="/year1">More</Card.Link>
                </Card.Body>
              </Card>
            </button>
            
            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>2017</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Black Plague</Card.Subtitle>
                  <Card.Text>
                  <p>Highest Mortality Rate: 731.9 / 100,000</p>
                  <p>Birth Rate: 60.2 per 1000</p>
                  <p>Most Infectious Disease: Black Plague</p>
                  <p>Deadliest Disease: Hepatitis A</p>
                  <p>Highest Infected Country: Madagascar</p>
                  </Card.Text>
                  <Card.Link className="red" href="/year2">More</Card.Link>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>2018</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">E. Coli</Card.Subtitle>
                  <Card.Text>
                  <p>Highest Mortality Rate: 67.8 deaths per 100,000</p>
                  <p>Birth Rate: 11.6 per 1000</p>
                  <p>Most Infectious Disease: E. Coli</p>
                  <p>Deadliest Disease: Ebola</p>
                  <p>Highest Infected Country: Africa</p>
                  </Card.Text>
                  <Card.Link className="red" href="/year3">More</Card.Link>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Title</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Subtitle</Card.Subtitle>
                  <Card.Text>
                    XXX
                  </Card.Text>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Title</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Subtitle</Card.Subtitle>
                  <Card.Text>
                    XXX
                  </Card.Text>
                </Card.Body>
              </Card>
            </button>

            <button className="marginSpace">
              <Card style={{ width: '18rem' }}>
                <Card.Body>
                  <Card.Title>Title</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Subtitle</Card.Subtitle>
                  <Card.Text>
                    XXX
                  </Card.Text>
                </Card.Body>
              </Card>
            </button>
          </div>
        </div>
      </div>
    )
  }
}
