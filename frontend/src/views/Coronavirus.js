import React from 'react';
import ReactTooltip from "react-tooltip";
import CoronaChart from "../components/CoronaChart.js";
import { StatsTable, StaticTable } from "../components/StatsTable.js";
import API_PATH from "../components/ApiPath.js";
import axios from "axios"
import IconButton from '@material-ui/core/IconButton';
import HelpIcon from '@material-ui/icons/Help';
import Tooltip from '@material-ui/core/Tooltip';

import 'intro.js/themes/introjs-modern.css';
import "../css/Page.css"

export default class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "GLOBAL",
      iso: "GLO",
      tooltip: "",
      slug: "GLO",
      disease_model: null,
      country_model: null,
      all_disease_stats: null,
      d_load: 0,
      c_load: 0,
      id: "Coronavirus Statistics"
    }
    this.updateData()
  }

  updateData() {
    function replaceString(str){
      if(!str.localeCompare("GLO"))
        return "world/total";
      console.log("past global with str: " + str)
      str = str.replace(/\s+/g, '-').toLowerCase();
        return "total/country/" + str;
    }

    if (this.state.iso === "GLO") {
      axios({
        method: "get",
        url: "https://api.covid19api.com/summary"
      }).then((response) => {
        this.setState({ all_disease_stats: response.data })
      }).catch((error) => {
        alert("all_disease_stats error")
      })
    }

    axios({
      method: "get",
      url: "https://api.covid19api.com/" + replaceString(this.state.slug)
    }).then((response) => {
      this.setState({ disease_model: response.data, d_load: 1 })
    }).catch((error) => {
      this.setState({ d_load: -1 })
    })

    console.log(this.state.disease_model)
    console.log(this.state.d_load)
  }

  setContent = (tooltip) => {
    this.setState({tooltip: tooltip})
  }

  setCountry = (title) => {
    this.setState({title: title.toUpperCase()})
    console.log("LOOK " + this.state.title)
  }

  setISO = (iso) => {
    this.setState({iso: iso.toUpperCase()}, this.updateData)
    console.log("HERE " + this.state.iso)
  }

  setSlug = (slug) => {
    this.setState({slug: slug})
    console.log("slug: " + slug)
  }

  formatContent(tooltip) {
    if (tooltip !== "")
      return tooltip.split('\n').map( (it, i) => <div key={'x'+i}>{it}</div> )
    else
      return tooltip
  }

  updateDiseaseStats() {

    function beautifyNumber(number, decimalCount, format) {
      let result = parseFloat(number).toFixed(decimalCount);
      let formattedResult = (format) ? result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        : (result)
      return formattedResult
    }

    var stats = []
    if (this.state.d_load === 1) {
      const model = this.state.disease_model
      const index = model.length-1
      if(!this.state.slug.localeCompare("GLO")){
        stats = stats.concat([
          { id: "Total Confirmed", val: beautifyNumber(model.TotalConfirmed, 0, true) },
          { id: "Total Deaths", val: beautifyNumber(model.TotalDeaths, 0, true)},
          { id: "Total Recovered", val: beautifyNumber(model.TotalRecovered, 0, true)}
        ])
       }
       else if(model[index]!==undefined){
        console.log("table for not world")
        stats = stats.concat([
          { id: "Confirmed", val: beautifyNumber(model[index].Confirmed, 0, true) },
          { id: "Deaths", val: beautifyNumber(model[index].Deaths, 0, true)},
          { id: "Recovered", val: beautifyNumber(model[index].Recovered, 0, true)},
          { id: "Date", val: "As of " + model[index].Date}
        ])

       }
    }
    return stats
  }

  render() {

    function fixBadCase(number) {
      if(number===(-999999))
        return ""
    }

    // var disease_path = "diseases/?cause=" + this.state.id + "&year=2017&location=" + this.state.iso
    const disease_title = this.state.title
    const country_title = "Country Data"
    const disease_header = [
      {title: "Statistics", field: "id"},
      {title: "(in persons)", field: "val"}
    ]

    var disease_stats = this.updateDiseaseStats()

    return (
      <div className="dark-back">
        <div className="container-fluid">
          <div className="row" style={{paddingLeft:'30px', paddingRight:'30px'}}>
            <div className="col-md-12 red topic-banner raleWay">
              <h2 id="letter-spacing" style={{paddingBottom: "15px"}}>
                {("COVID-19").toUpperCase().replace(/_/g, " ")}
                <Tooltip title="Credits">
                  <IconButton
                    onClick={() => window.location.href = "https://covid19api.com"}
                    className="pull-left"
                    aria-label="Data Source"
                    size="small"
                  >
                    <HelpIcon style={{color: "white"}}/>
                  </IconButton>
                </Tooltip>
              </h2>
            </div>
          </div>
          <div className="row" style={{padding:'30px'}}>
            <div className="disease-table col-sm-12 col-md-4 outline red" style={{paddingLeft:"30px", paddingRight:"30px"}}>
              <h3 className="raleWay topic-banner" id="letter-spacing"> { this.state.title } </h3>
              {StaticTable("COVID-19", disease_header, disease_stats, 12, [4, 8, 12], false)}
            </div>
            <div className="map col-sm-12 col-md-8 outline" style={{paddingLeft:'5px', paddingRight:'5px'}}>
              {<CoronaChart setTooltipContent={this.setContent} setCountry={this.setCountry} setISO={this.setISO}
                setSlug={this.setSlug} data={this.state.all_disease_stats}/>}

              <ReactTooltip multiline={true}>{this.formatContent(this.state.tooltip)}</ReactTooltip>
            </div>
          </div>

        </div>
      </div>
    )
  }
}
