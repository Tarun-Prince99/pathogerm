import React from "react";
import axios from "axios"
import API_PATH from "../components/ApiPath.js"
import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, AreaChart, Area, ComposedChart, Line,
} from 'recharts';


import '../css/Text.css';
import '../css/Page.css';

export default class Visualizations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        model: null,
        oneModel: null,
        threeModel: null,
        oneLoad: 0,
        twoLoad: 0,
        threeLoad: 0,
    }

      axios({
        method: "get",
        url: 'https://api.landitinspace.net/api/countries?page=1'
      }).then((response) => {
        this.setState({ oneModel: response.data, oneLoad: 1 })
      }).catch((error) => {
        this.setState({ oneLoad: -1 })
      })

      axios({
        method: "get",
        url: 'https://api.landitinspace.net/api/astronauts?page=1'
      }).then((response) => {
        this.setState({ twoModel: response.data, twoLoad: 1 })
      }).catch((error) => {
        this.setState({ twoLoad: -1 })
      })

      axios({
        method: "get",
        url: 'https://api.landitinspace.net/api/satellites?page=1'
      }).then((response) => {
        this.setState({ threeModel: response.data, threeLoad: 1 })
      }).catch((error) => {
        this.setState({ threeLoad: -1 })
      })
  }

  loading() {
    return <div className="content-center"><h1 id="letter-spacing">LOADING...</h1></div>
  }

  render() {
    var OneArray = []
    if (this.state.oneLoad <= 0 || this.state.twoLoad <= 0 || this.state.threeLoad <= 0){
      return this.loading()
    }
    else{
      //first visualization
      for (var row in this.state.oneModel.objects){
        // console.log(this.state.oneModel.objects[row])
        if(this.state.oneModel.objects[row].count_astronauts > 0){
          let item = {};
          item["Country"] = this.state.oneModel.objects[row].country
          item["Astronauts"] = this.state.oneModel.objects[row].count_astronauts
          item["Satellites"] = this.state.oneModel.objects[row].count_satellites
          item["Ratio"] = this.state.oneModel.objects[row].count_satellites/this.state.oneModel.objects[row].count_astronauts
          OneArray.push(item)
        }
      }
      //second visualization
      var twoArray = []
      let countries = {}
      for (var row in this.state.twoModel.objects){
        // console.log(this.state.threeModel.objects[row].launch_site)
        let country = this.state.twoModel.objects[row].country
        if(country in countries){
          if (this.state.twoModel.objects[row].gender == "Man"){
            countries[country][0] = countries[country][0] + 1
          } else {
            countries[country][1] = countries[country][1] + 1
          }
        }
        else{
          if (this.state.twoModel.objects[row].gender == "Man"){
            countries[country] = [1, 0]
          } else {
            countries[country] = [0, 1]
          }
        }
        if (country == "Japan") {
          console.log(countries[country])
        }
      }
      for (var country in countries){
        // if (countries[country][0] != 0 && countries[country][1] != 0) {
          let item = {};
          item["Country"] = country
          item["Male"] = countries[country][0]
          item["Female"] = countries[country][1]
          twoArray.push(item)
        // }
      }
      console.log(twoArray)
      //third visualization
      var threeArray = []
      let launchList = {}
      for (var row in this.state.threeModel.objects){
        // console.log(this.state.threeModel.objects[row].launch_site)
        let launchSite = this.state.threeModel.objects[row].launch_site
        if(launchSite in launchList){
          launchList[launchSite] = launchList[launchSite] + 1
        }
        else{
          launchList[launchSite] = 1
        }
      }
      for (var index in launchList){
        let item = {};
        item["Launch Site"] = index
        item["Number of Launches"] = launchList[index]
        threeArray.push(item)
      }
    }
    return (
      <div className="dark-back">
        <div className="container formPad raleWay">
          <h1 id="letter-spacing" className="red" style={{paddingBottom: '35px'}}>VISUALIZATIONS</h1>
          <div className="row" style={{padding: "30px", display: "inline-block", width: "100%"}}>
            <h5 className="white">Number of Astronauts and Satellites by Country</h5>
            <ComposedChart
            width={1000}
            height={600}
            data={OneArray}
            margin={{
              top: 50, right: 30, left: 20, bottom: 80,
            }}
            style={{color: "white"}}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="Country" />
              <YAxis />
              <Tooltip contentStyle={{backgroundColor: "#000"}}/>
              <Legend />
              <Line type='monotone' dataKey="Satellites" fill="#e63333" />
              <Area type='monotone' dataKey="Astronauts" fill="#82ca9d" />
              <Bar dataKey="Ratio" fill="#8884d8" />
            </ComposedChart>

            <h5 className="white">Number of Satellite Launches by Launch Site</h5>
            <AreaChart
            width={1000}
            height={600}
            data={threeArray}
            margin={{
              top: 50, right: 30, left: 20, bottom: 80,
            }}
            style={{color: "white"}}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="Launch Site" />
              <YAxis />
              <Tooltip contentStyle={{backgroundColor: "#000"}}/>
              <Legend />
              <Area type='monotone' dataKey="Number of Launches" fill="#8884d8" />
            </AreaChart>

            <h5 className="white">Number of Male and Female Astronauts by Country</h5>
            <BarChart
            width={1000}
            height={600}
            data={twoArray}
            margin={{
              top: 50, right: 30, left: 20, bottom: 80,
            }}
            style={{color: "white"}}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="Country" />
              <YAxis />
              <Tooltip contentStyle={{backgroundColor: "#000"}}/>
              <Legend />
              <Bar dataKey="Male" fill="#8884d8" />
              <Bar dataKey="Female" fill="#82ca9d" />
            </BarChart>
          </div>
        </div>
      </div>
    )
  }
}
