import React from "react";
import axios from "axios"
import API_PATH from "../components/ApiPath.js"
import isoCountries from "../components/CountryMappings.js"
import IconButton from '@material-ui/core/IconButton';
import MapIcon from '@material-ui/icons/Map';
import InfoIcon from '@material-ui/icons/Info';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import Tooltip from '@material-ui/core/Tooltip';

import '../css/Text.css';
import '../css/Page.css';

export default class Information extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model: null,
      load: 0,
      isCountry: -1,
      info_load: 0,
      information: null,
      wiki_model: null,
      wiki_load: 0,
      id: this.props.match.params.id
    }

    axios({
      method: "get",
      url: API_PATH + "diseases/?cause=" + this.state.id
    }).then((response) => {
      this.setState({ model: response.data, load: 1 })
      this.isCountry(this.state.id)
      this.loadInformation(this.state.isCountry)
    }).catch((error) => {
      this.setState({ load: -1 })
    })


  }

  isCountry(name) {
    for (var key in isoCountries) {
      if (isoCountries[key] === name) {
        this.setState({country_name: this.state.id, id: key, isCountry: 1})
        return
      }
    }
    this.setState({isCountry: 0})
    return
  }

  loadDescription(url) {
    axios({
      method: "get",
      url: url
    }).then((response) => {
      this.setState({ wiki_model: response.data, wiki_load: 1 })
    }).catch((error) => {
      this.setState({ wiki_load: -1 })
    })
  }

  loadInformation(isCountry) {
    console.log("isCountry: " + isCountry)
    let attribute = isCountry ? "location" : "cause";
    let exclude = isCountry ? "all_causes" : "global"
    let prevalenceUrl = `${API_PATH}diseases/?measure=Prevalence&${exclude}=false&ordering=-number&${attribute}=${this.state.id}`
    let incidenceUrl = `${API_PATH}diseases/?measure=Incidence&${exclude}=false&ordering=-number&${attribute}=${this.state.id}`
    let dalysUrl = `${API_PATH}diseases/?measure=DALYs (Disability-Adjusted Life Years)&${exclude}=false&ordering=-number&${attribute}=${this.state.id}`
    let deathsUrl = `${API_PATH}diseases/?measure=Deaths&${exclude}=false&ordering=-number&${attribute}=${this.state.id}`
    axios.all([
      axios.get(prevalenceUrl),
      axios.get(incidenceUrl),
      axios.get(dalysUrl),
      axios.get(deathsUrl)]
    ).then(axios.spread((...responses) => {
      var information = this.getInformation(responses, isCountry)
      this.setState({information: information, info_load: 1 })
      console.log(this.state.information)
    })).catch((error) => {
      this.setState({ info_load: -1 })
    })
  }

  getInformation(responses, isCountry) {
    let info = []
    for (var index = 0; index < responses.length; index++) {
      console.log(responses[index])
      var results = responses[index].data.results
      if (results.length != 0) {
        if (isCountry) {
          info.push({title: results[0].measure, number: results[0].number, disease: results[0].cause, year: results[0].year})
        } else {
          info.push({title: results[0].measure, number: results[0].number, country: isoCountries[results[0].location], year: results[0].year})
        }
      }
    }
    return info
  }

  loading() {
    return <div className="content-center"><h1 id="letter-spacing">LOADING...</h1></div>
  }

  render() {
    var extract = "n/a"
    var sampleImage = null
    var title = "n/a"
    var description = "n/a"
    var wiki_link = "n/a"

    if (this.state.load <= 0) {
      return this.loading()
    } else if (this.state.model.count === 0) {
      this.loadDescription("https://en.wikipedia.org/api/rest_v1/page/summary/" + this.state.country_name)
    } else {
      this.loadDescription(this.state.model.results[0].description)
    }
    if(this.state.wiki_load <= 0) {
      return this.loading()
    }

    function beautifyNumber(number) {
      let result = parseFloat(number).toFixed(3);
      return result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    extract = this.state.wiki_model.extract
    if(this.state.wiki_model.thumbnail)
      sampleImage = this.state.wiki_model.thumbnail.source
    title = (this.state.wiki_model.title).toUpperCase()
    description = this.state.wiki_model.description
    wiki_link = this.state.wiki_model.content_urls.desktop.page

    let result;
    if (this.state.info_load <= 0) {
      return this.loading()
    }
    var c = 0
    if (this.state.isCountry) {
      result = this.state.information.map((data) => {
      c+=1

        return (<div className="col-md-3">
            <h5 id="letter-spacing" className="red" style={{paddingBottom: "3%"}}>{data.title.toUpperCase()}</h5>
            <div key = {c}>
            <Tooltip title="View Disease Information">
            <IconButton
              onClick={() => {this.props.history.push("/information/" + data.disease); window.location.reload()}}
              className="pull-left"
              aria-label="View Disease Information"
              size="small"
              >
              <InfoIcon style={{color: "white"}}/>
              </IconButton>
              </Tooltip>
            <Tooltip title="View on Timeline">
              <IconButton
              onClick={() => {this.props.history.push("/timeline/" + data.year)}}
              className="pull-left"
              aria-label="View on Timeline"
              size="small"
              >
              <AccessTimeIcon style={{color: "white"}}/>
              </IconButton>
              </Tooltip>
            <p>{data.disease}</p>
            <p style={{fontFamily: "Arial"}}>{beautifyNumber(data.number)}</p>
            <p style={{fontFamily: "Arial"}}>{data.year}</p>
           </div>
          </div>)
      })
    } else {
      result = this.state.information.map((data) => {
        return (
          <div className="col-md-3">
          <div key = {c}>
            <Tooltip title="View Country Information">
            <IconButton
              onClick={() => {this.props.history.push("/information/" + data.country); window.location.reload()}}
              className="pull-left"
              aria-label="View Country Information"
              size="small"
              
              >
              <InfoIcon style={{color: "white"}}/>
              </IconButton>
              </Tooltip>
              
            <Tooltip title="View on Timeline">
              <IconButton
              onClick={() => {this.props.history.push("/timeline/" + data.year)}}
              className="pull-left"
              aria-label="View on Timeline"
              size="small"
              >
              <AccessTimeIcon style={{color: "white"}}/>
              </IconButton>
              </Tooltip>

            <h5 id="letter-spacing" className="red" style={{paddingBottom: "3%"}}>{data.title.toUpperCase()}</h5>
            <p style={{fontFamily: "Arial"}}>{beautifyNumber(data.number)}</p>
            <p>{data.country}</p>
            <p style={{fontFamily: "Arial"}}>{data.year}</p>
          </div>
          </div>
        )
      })
    }

    return (
      <div className="dark-back">
        <div className="container-fluid formPad raleWay white">
          <h1 id="letter-spacing" className="red" style={{paddingBottom: '35px'}}><b>{title}</b></h1>
          <div className="container" style={{paddingLeft: "20%", paddingRight: "20%", paddingBottom: "2%"}}>
            <img src={sampleImage} alt={title}/>
            <div className = "description">
              <i><p style={{paddingTop: "10px"}}>{description}</p></i>
              <a href={wiki_link}>source</a>
            </div>
          </div>
          <div className="container outline" style={{paddingTop: "1%"}}>
            <p>{extract}</p>
          </div>
          {<h2 id="letter-spacing" className="red" style={{paddingTop: "4%", paddingBottom: "5px"}}>RECORD HIGH MEASUREMENTS</h2>}
          <div className="row formPad">
            {result}
          </div>
        </div>
      </div>
    )
  }
}
