import React from "react";
import axios from "axios"
import API_PATH from "../components/ApiPath.js"
import { StatsTable } from "../components/StatsTable.js";
import CountryInformationCard from "../components/CountryInformationCard.js"

import '../css/Text.css';
import '../css/Page.css';

export default class TimelineList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        model: null,
        load: 0,
    }

    axios({
        method: "get",
        url: API_PATH + "years/?name=USA&limit=100&ordering=-year"
      }).then((response) => {
        this.setState({ model: response.data, load: 1 })
        console.log(this.state.model)
      }).catch((error) => {
        this.setState({ load: -1 })
      })
  }

  loading() {
    return <div className="content-center"><h1 id="letter-spacing">LOADING...</h1></div>
  }

  render() {

    function beautifyNumber(number) {
      let result = parseFloat(number).toFixed(3);
      return result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    const header = [
      {title: "Year", field: "year", customSort: (a, b) => a.year.props.children.toString().localeCompare(b.year.props.children.toString())},
      {title: "Physicians (per 10,000)", field: "med_doctors"},
      {title: "Infectious Diseases Percentage", field: "uhc_infect", type: "numeric", render: rowData => rowData.uhc_infect ? rowData.uhc_infect + "%" : ""},
      {title: "Life Expectancy", field: "life_expect", type: "numeric"},
      {title: "Health Expenditure per GDP", field: "health_per_gdp", type: "numeric"},
    ]

    var year_path = "years/?name=USA&limit=100&ordering=-year";

    if (this.state.load < 1) {
      return this.loading()
    }
    let list;
    list = this.state.model.results.map((data) => {
      console.log(data);
      return <CountryInformationCard data={data}/>
    })
    return (
      <div className="dark-back">
        <div className="container formPad raleWay white">
        <h1 id="letter-spacing" className="red" style={{paddingBottom: '35px'}}>TIMELINE</h1>
          {StatsTable("Year Data", header, 10, year_path, false, ["year"], 1200)}
        </div>
      </div>
    )
  }
}
