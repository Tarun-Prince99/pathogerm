import React from "react"
import { StatsTable } from "../components/StatsTable.js";
import Highlighter from "react-highlighter"

import "../css/Page.css"
import "../css/Text.css"

class Diseases extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model: null,
      load: 0
    }
  }

  render() {

    function beautifyNumber(number) {
      let result = parseFloat(number).toFixed(3);
      return result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    const header = [
      {title: "Cause", field: "cause", customSort: (a, b) => a.cause.props.children.localeCompare(b.cause.props.children)},
      {title: "Measure", field: "measure", customSort: (a, b) => a.measure.props.children.localeCompare(b.measure.props.children)},
      {title: "Number", field: "number", type: "numeric", render: rowData => beautifyNumber(rowData.number)},
      {title: "Percent", field: "percent", type: "numeric", render: rowData => parseFloat(rowData.percent).toFixed(5)},
      {title: "Rate", field: "rate", type: "numeric", render: rowData => beautifyNumber(rowData.rate)},
    ]

    var disease_path = "diseases/?year=2017&location=GLO"

    return (
      <div className="dark-back">
        <div className="container formPad raleWay white">
          <h1 id="letter-spacing" className="red" style={{paddingBottom: '35px'}}>DISEASES</h1>
          {StatsTable("Disease Data", header, 10, disease_path, true, ["cause", "measure"], 1200)}
        </div>
      </div>
    );
  }
}

export default Diseases;