import React from "react";
import axios from "axios"
import API_PATH from "../components/ApiPath.js";
import Slider from "@material-ui/core/Slider";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import {DataChart} from "../components/DataChart.js"
import { StatsTable } from "../components/StatsTable.js";
import IconButton from '@material-ui/core/IconButton';
import MapIcon from '@material-ui/icons/Map';
import InfoIcon from '@material-ui/icons/Info';
import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, AreaChart, Area, ComposedChart, Line,
} from 'recharts';

import '../css/Text.css';
import '../css/Page.css';

const redColor = "#DD2C06"
const darkColor = "#202020"
const theme = createMuiTheme({
  overrides: {
    MuiSlider: {
      root: {
        backgroundColor: darkColor,
        color: redColor
      },
      thumb: {
        "&:hover": {
          boxShadow: "0px 0px 0px 8px rgba(221, 44, 6, .16)"
        },
        "&:active": {
          boxShadow: "0px 0px 0px 14px rgba(221, 44, 6, .16)"
        }
      },
      markLabel: {
        color: "rgba(255, 255, 255, .55)"
      },
      markLabelActive: {
        color: "rgba(255, 255, 255, 1)"
      }
    }
  }
});

export default class Timeline extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id &&
        (this.props.match.params.id >= 1990 && this.props.match.params.id <= 2017)
        ? this.props.match.params.id : "2015",
      model: null,
      load: 0,
      prevalence_model: null,
      prev_load: 0,
      incidence_model: null,
      inc_load: 0,
      deaths_model: null,
      deaths_load: 0,
      dalys_model: null,
      dalys_load: 0
    }
    this.updateYear(this.state.id);
  }

  updateYear(year) {
    this.props.history.push("/timeline/" + year)
    axios({
      method: "get",
      url: API_PATH + "years/?year=" + year + "&limit=300"
    }).then((response) => {
      this.setState({ model: response.data, load: 1, id: year})
      console.log(response.data)
    }).catch((error) => {
      this.setState({ load: -1 })
    })

    // Top Prevalence
    axios({
      method: "get",
      url: API_PATH + "diseases/?all_causes=false&measure=Prevalence&location=GLO&ordering=-rate&limit=10&year=" + year
    }).then((response) => {
      this.setState({ prevalence_model: response.data.results, prev_load: 1})
      console.log(response.data.results)
    }).catch((error) => {
      this.setState({ prev_load: -1 })
    })

    // Top Incidence
    axios({
      method: "get",
      url: API_PATH + "diseases/?all_causes=false&measure=Incidence&location=GLO&ordering=-rate&limit=10&year=" + year
    }).then((response) => {
      this.setState({ incidence_model: response.data.results, inc_load: 1})
      console.log(response.data.results)
    }).catch((error) => {
      this.setState({ inc_load: -1 })
    })

    // Top Deaths
    axios({
      method: "get",
      url: API_PATH + "diseases/?all_causes=false&measure=Deaths&location=GLO&ordering=-rate&limit=10&year=" + year
    }).then((response) => {
      this.setState({ deaths_model: response.data.results, deaths_load: 1})
      console.log(response.data.results)
    }).catch((error) => {
      this.setState({ deaths_load: -1 })
    })

    // Top DALYs
    axios({
      method: "get",
      url: API_PATH + "diseases/?all_causes=false&measure=DALYs (Disability-Adjusted Life Years)&location=GLO&ordering=-rate&limit=10&year=" + year
    }).then((response) => {
      this.setState({ dalys_model: response.data.results, dalys_load: 1})
      console.log(response.data.results)
    }).catch((error) => {
      this.setState({ dalys_load: -1 })
    })
  }

  render() {

    if (this.state.load <= 0) {
      return <div className="content-center"><h1 id="letter-spacing">LOADING...</h1></div>
    }

    if(this.state.prev_load === 0 || this.state.inc_load === 0 || this.state.deaths_load === 0 || this.state.dalys_load === 0)
      return <div className="content-center"><h1 id="letter-spacing">LOADING...</h1></div>
    if(this.state.prev_load === -1 || this.state.inc_load === -1 || this.state.deaths_load === -1 || this.state.dalys_load === -1)
      return <div className="content-center"><h1 id="letter-spacing">FAILED TO LOAD</h1></div>

    function beautifyNumber(number) {
      let result = parseFloat(number).toFixed(3);
      return result.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    var c = 0
    const prevalence_results = this.state.prevalence_model.map(result => {
      c += 1
      return (
        <div key={c}>
          <IconButton
            onClick={() => {this.props.history.push("/map/" + result.cause)}}
            className="pull-left"
            aria-label="View Map"
            size="small"
          >
            <MapIcon style={{color: "white"}}/>
          </IconButton>
          <IconButton
            onClick={() => {this.props.history.push("/information/" + result.cause)}}
            className="pull-left"
            aria-label="More Information"
            size="small"
          >
            <InfoIcon style={{color: "white"}}/>
          </IconButton>
          <span>{result.cause}</span>
        </div>
      )
    })
    const incidence_results = this.state.incidence_model.map(result => {
      c += 1
      return (
        <div key={c}>
          <IconButton
            onClick={() => {this.props.history.push("/map/" + result.cause)}}
            className="pull-left"
            aria-label="View Map"
            size="small"
          >
            <MapIcon style={{color: "white"}}/>
          </IconButton>
          <IconButton
            onClick={() => {this.props.history.push("/information/" + result.cause)}}
            className="pull-left"
            aria-label="More Information"
            size="small"
          >
            <InfoIcon style={{color: "white"}}/>
          </IconButton>
          <span>{result.cause}</span>
        </div>
      )
    })
    const deaths_results = this.state.deaths_model.map(result => {
      c += 1
      return (
        <div key={c}>
          <IconButton
            onClick={() => {this.props.history.push("/map/" + result.cause)}}
            className="pull-left"
            aria-label="View Map"
            size="small"
          >
            <MapIcon style={{color: "white"}}/>
          </IconButton>
          <IconButton
            onClick={() => {this.props.history.push("/information/" + result.cause)}}
            className="pull-left"
            aria-label="More Information"
            size="small"
          >
            <InfoIcon style={{color: "white"}}/>
          </IconButton>
          <span>{result.cause}</span>
        </div>
      )
    })
    const dalys_results = this.state.dalys_model.map(result => {
      c += 1
      return (
        <div key={c}>
          <IconButton
            onClick={() => {this.props.history.push("/map/" + result.cause)}}
            className="pull-left"
            aria-label="View Map"
            size="small"
          >
            <MapIcon style={{color: "white"}}/>
          </IconButton>
          <IconButton
            onClick={() => {this.props.history.push("/information/" + result.cause)}}
            className="pull-left"
            aria-label="More Information"
            size="small"
          >
            <InfoIcon style={{color: "white"}}/>
          </IconButton>
          <span>{result.cause}</span>
        </div>
      )
    })

    var myMap = new Map();
    myMap.set("", 0);
    myMap.set("Low", 2);
    myMap.set("Middle", 4);
    myMap.set("High", 6);
    console.log(this.state.disease_path);

    function compare(a, b) {
      let comparison = 0;
      if (a < b) {
        comparison = -1;
      }
      else if (a > b) {
        comparison = 1;
      }

      return comparison;
    }

    const marks = [
      {value:1990, label:"1990"}, {value:1991, label:"1991"}, {value:1992, label:"1992"},
      {value:1993, label:"1993"}, {value:1994, label:"1994"}, {value:1995, label:"1995"},
      {value:1996, label:"1996"}, {value:1997, label:"1997"}, {value:1998, label:"1998"},
      {value:1999, label:"1999"}, {value:2000, label:"2000"}, {value:2001, label:"2001"},
      {value:2002, label:"2002"}, {value:2003, label:"2003"}, {value:2004, label:"2004"},
      {value:2005, label:"2005"}, {value:2006, label:"2006"}, {value:2007, label:"2007"},
      {value:2008, label:"2008"}, {value:2009, label:"2009"}, {value:2010, label:"2010"},
      {value:2011, label:"2011"}, {value:2012, label:"2012"}, {value:2013, label:"2013"},
      {value:2014, label:"2014"}, {value:2015, label:"2015"}, {value:2016, label:"2016"},
      {value:2017, label:"2017"}
    ]

    return (
      <div className="dark-back" style={{paddingLeft: "10%", paddingRight: "10%"}}>
        <div className="container-fluid formPad raleWay">
          <h1 id="letter-spacing" className="red">TIMELINE</h1>
          <div className="row" style={{padding: "5%", color: "white"}}>
            <MuiThemeProvider theme={theme}>
              <Slider
                defaultValue={this.state.id}
                aria-labelledby="discrete-slider"
                valueLabelDisplay="on"
                step={1}
                marks={marks}
                min={1990}
                max={2017}
                track={false}
                onChangeCommitted={(event, value) => this.updateYear(value)}
              />
            </MuiThemeProvider>
          </div>
          <div>
            <h3 id="letter-spacing" className="red">TOP DISEASES IN {this.state.id}</h3>
            <div className="row formPad white left">
              <div className="col-md-3 col-sm">
                <h3 id="letter-spacing" className="red" style={{paddingBottom: "10px"}}>PREVALENCE:</h3>
                {prevalence_results.length > 0 ? prevalence_results : <p className="red">Results not found.</p>}
              </div>
              <div className="col-md-3 col-sm">
                <h3 id="letter-spacing" className="red" style={{paddingBottom: "10px"}}>INCIDENCE:</h3>
                {incidence_results.length > 0 ? incidence_results : <p className="red">Results not found.</p>}
              </div>
              <div className="col-md-3 col-sm">
                <h3 id="letter-spacing" className="red" style={{paddingBottom: "10px"}}>DEATHS:</h3>
                {deaths_results.length > 0 ? deaths_results : <p className="red">Results not found.</p>}
              </div>
              <div className="col-md-3 col-sm">
                <h3 id="letter-spacing" className="red" style={{paddingBottom: "10px"}}>DALYs:</h3>
                {dalys_results.length > 0 ? dalys_results : <p className="red">Results not found.</p>}
              </div>
            </div>
          </div>
          <div className="row" style={{padding: "30px", display: "inline-block"}}>
          <AreaChart
            width={1000}
            height={600}
            padding={30}
            data={this.state.model.results.sort((a, b) => b.med_doctors -
              a.med_doctors).slice(0, 100).sort((a, b) => compare(a.name, b.name))}
            margin={{
              top: 5, right: 30, left: 20, bottom: 5,
            }}
            style = {{
              color:"#fff"
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="med_doctors" />
            <YAxis />
            <Tooltip contentStyle={{ backgroundColor: '#000000' }} />
            <Legend />
            <Area name = 'Physicians per 10,000 people' type='monotone' dataKey="med_doctors"fill="#DD2C06"/>
          </AreaChart>
          </div>
          {DataChart("Infectious Diseases Percentage", "uhc_infect", this.state.model.results.sort((a, b) => b.uhc_infect -
              a.uhc_infect).slice(0, 100).sort((a, b) => compare(a.name, b.name)))}
          {DataChart("Noncommunicable Diseases Prevalence", "uhc_noncom", this.state.model.results.sort((a, b) => b.uhc_noncom -
              a.uhc_noncom).slice(0, 100).sort((a, b) => compare(a.name, b.name)))}
          {DataChart("Noncommunicable Diseases Deaths", "deaths_noncom", this.state.model.results.sort((a, b) => b.deaths_noncom -
              a.deaths_noncom).slice(0, 100).sort((a, b) => compare(a.name, b.name)))}
          {DataChart("Life Expectancy", "life_expect", this.state.model.results.sort((a, b) => b.life_expect -
              a.life_expect).slice(0, 100).sort((a, b) => compare(a.name, b.name)))}
          {DataChart("Population Prevalence of Depression", "est_preval_dep", this.state.model.results.sort((a, b) => b.est_preval_dep -
              a.est_preval_dep).slice(0, 100).sort((a, b) => compare(a.name, b.name)))}
          {DataChart("Health Expenditure per GDP", "health_per_gdp", this.state.model.results.sort((a, b) => b.health_per_gdp -
              a.health_per_gdp).slice(0, 100).sort((a, b) => compare(a.name, b.name)))}
          {DataChart("Government Health Budget (USD)", "health_budg", this.state.model.results.sort((a, b) => b.health_budg -
              a.health_budg).slice(0, 100)  .sort((a, b) => compare(a.name, b.name)))}
          {DataChart("Income Level", "income_levl", this.state.model.results.map(function(item){
            item.income_levl = myMap.get(item.income_levl);
            return item;
            }))
          }
        </div>
      </div>
    )
  }
}
