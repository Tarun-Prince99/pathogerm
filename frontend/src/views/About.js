import React from 'react';

import '../css/Text.css';
import '../css/Page.css';
import '../css/About.css';

import Brian from '../images/brian.png';
import Michael from '../images/michael_edit.png';
import Tarun from '../images/tarun.jpg';
import Darren from '../images/darren.jpeg';
import Paul from '../images/paul.jpeg';
import List from '../images/test.png'
import Test from '../images/chemistry.png'

var GITLAB_API_URL = 'https://gitlab.com/api/v4/projects/17076272';
var GITLAB_SVG = 'https://upload.wikimedia.org/wikipedia/commons/1/18/GitLab_Logo.svg'
var REPO_URL = 'https://gitlab.com/Tarun-Prince99/pathogerm'
var ISSUES_URL = REPO_URL + '/issues'
var WORLD_BANK = 'https://datahelpdesk.worldbank.org/knowledgebase/articles/889392-about-the-indicators-api-documentation'
var WHO_API = 'https://www.who.int/data/gho/info/athena-api'
var GBD_DATA = 'http://ghdx.healthdata.org/gbd-results-tool'
var REACT_LINK = 'https://reactjs.org/'
var BOOTSTRAP_LINK = 'https://getbootstrap.com/'
var DJANGO_LINK = 'https://www.djangoproject.com/'
var DJANGO_REST_LINK = 'https://www.django-rest-framework.org/'
var POSTGRES_LINK = 'https://www.postgresql.org/'
var AWS_LINK = 'https://aws.amazon.com/'
var GITLAB_LINK = 'https://gitlab.com/'
var SELENIUM_LINK = 'https://www.selenium.dev/'
var MOCHA_LINK = 'https://mochajs.org/'
var POSTMAN_LINK = 'https://www.postman.com/'
var ISSUES_SVG = List
var TESTS_SVG = Test

export default class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      issues: []
    }
  }

  componentDidMount() {
    this.getData();
  }

  render() {
    return (
      <div className="container" style={{position: "relative"}}>
        <div className="container formPad raleWay">
          <h1 id="letter-spacing" style={{paddingBottom: "35px"}}>ABOUT PATHOGERM</h1>
          <p>With the evolution of new infectious diseases alongside the modern epidemiological transitions of illness, it is imperative to spread awareness about disease transmission and the associated prevention in an increasingly globalized world. The goal of Pathogerm is to provide an intuitive platform that provides insight on various non-communicable and infectious diseases across different parts of the world. </p>
          <h1 id="letter-spacing" style={{paddingBottom: "35px", paddingTop: "20px"}}>MEET THE TEAM</h1>

          {/* Team members */}
          <div className="row">
            <div className="col-md-6 tilePad" style={{paddingLeft: "100px"}}>
            <a href = "https://brianlequang.com">
              <img src={Brian} alt="Brian LeQuang" className="teamPic"/>
            </a>
              <h3>Brian LeQuang</h3>
              <p>
                Frontend
              </p>
              <p>
                I am a machine learning enthusiast, a hobbyist programmer, and a computer science student at the University of Texas at Austin. I worked primarily on the design and construction of the frontend.
              </p>
              <div className="container d-flex">
                <div className="logoWrapper">
                  <a href={REPO_URL}>
                    <img className="gitlabLogo" src={GITLAB_SVG} alt='GitLab'></img>
                  </a>
                  <p>{this.state.users[0]}</p>
                </div>
                <div className="logoWrapper">
                  <a href={ISSUES_URL}>
                    <img className="issuesLogo" src={ISSUES_SVG} alt='Issues'></img>
                  </a>
                  <p>{this.state.issues[0]}</p>
                </div>
                <div className="logoWrapper">
                  <img className="testsLogo" src={TESTS_SVG} alt='Tests'></img>
                  <p>0</p>
                </div>
              </div>

            </div>
            <div className="col-md-6 tilePad" style={{paddingRight: "100px"}}>
            <a href = "https://academic.oup.com/bioinformatics/article/doi/10.1093/bioinformatics/btaa028/5701654">
              <img src={Michael} alt="Michael Geng" className="teamPic"/>
            </a>
              <h3>Michael Geng</h3>
              <p>
                Backend
              </p>
              <p>
                I am a computer science and computational biology double major at the University of Texas at Austin who is interested in bioinformatics and genomics.
                I was involved in data curation, database design, and backend testing.
              </p>
              <div className="container d-flex">
                <div className="logoWrapper">
                  <a href={REPO_URL}>
                    <img className="gitlabLogo" src={GITLAB_SVG} alt='GitLab'></img>
                  </a>
                  <p>{this.state.users[1]}</p>
                </div>
                <div className="logoWrapper">
                  <a href={ISSUES_URL}>
                    <img className="issuesLogo" src={ISSUES_SVG} alt='Issues'></img>
                  </a>
                  <p>{this.state.issues[1]}</p>
                </div>
                <div className="logoWrapper">
                  <img className="testsLogo" src={TESTS_SVG} alt='Tests'></img>
                  <p>20</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4 tilePad">
            <a href = "https://gitlab.com/Tarun-Prince99">
              <img src={Tarun} alt="Tarun Prince" className="teamPic"/>
            </a>
              <h3>Tarun Prince</h3>
              <p>
                Full Stack
              </p>
              <p>
                I am a third year CS major at the University of Texas at Austin, a basketball aficionado, and am interested in Machine learning. I was involved in front end development and web scraping for this project.
              </p>
              <div className="container d-flex">
                <div className="logoWrapper">
                  <a href={REPO_URL}>
                    <img className="gitlabLogo" src={GITLAB_SVG} alt='GitLab'></img>
                  </a>
                  <p>{this.state.users[2]}</p>
                </div>
                <div className="logoWrapper">
                  <a href={ISSUES_URL}>
                    <img className="issuesLogo" src={ISSUES_SVG} alt='Issues'></img>
                  </a>
                  <p>{this.state.issues[2]}</p>
                </div>
                <div className="logoWrapper">
                  <img className="testsLogo" src={TESTS_SVG} alt='Tests'></img>
                  <p>2</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 tilePad">
              <img src={Darren} alt="Darren Phua" className="teamPic"/>
              <h3>Darren Phua</h3>
              <p>
                Backend
              </p>
              <p>
                I am a computer science major at the University of Texas at Austin. I mainly focus on backend and database design and I help out from time to time with the frontend.
              </p>
              <div className="container d-flex">
                <div className="logoWrapper">
                  <a href={REPO_URL}>
                    <img className="gitlabLogo" src={GITLAB_SVG} alt='GitLab'></img>
                  </a>
                  <p>{this.state.users[3]}</p>
                </div>
                <div className="logoWrapper">
                  <a href={ISSUES_URL}>
                    <img className="issuesLogo" src={ISSUES_SVG} alt='Issues'></img>
                  </a>
                  <p>{this.state.issues[3]}</p>
                </div>
                <div className="logoWrapper">
                  <img className="testsLogo" src={TESTS_SVG} alt='Tests'></img>
                  <p>15</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 tilePad">
              <img src={Paul} alt="Paul Sathuluri" className="teamPic"/>
              <h3>Paul Sathuluri</h3>
              <p>
                Full Stack
              </p>
              <p>
                I worked on Frontend Development & Testing. Stack Overflow Enthusiast @ UT Austin with interests in Applied Statistics.
              </p>
              <div className="container d-flex">
                <div className="logoWrapper">
                  <a href={REPO_URL}>
                    <img className="gitlabLogo" src={GITLAB_SVG} alt='GitLab'></img>
                  </a>
                  <p>{this.state.users[4]}</p>
                </div>
                <div className="logoWrapper">
                  <a href={ISSUES_URL}>
                    <img className="issuesLogo" src={ISSUES_SVG} alt='Issues'></img>
                  </a>
                  <p>{this.state.issues[4]}</p>
                </div>
                <div className="logoWrapper">
                  <img className="testsLogo" src={TESTS_SVG} alt='Tests'></img>
                  <p>15</p>
                </div>
              </div>
            </div>
          </div>

          {/* Git Statistics */}
          <h1 id="letter-spacing" style={{paddingBottom: '35px'}}>STATISTICS</h1>
          <div className="row">
            <div className="col-md-4">
              <div className="statsWrapper" style={{display: 'inline-flex'}}>
                <a href={REPO_URL}>
                  <img className="gitlabLogo" src={GITLAB_SVG} alt='GitLab'></img>
                </a>
                <p className="stats">{this.state.users[5]}</p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="statsWrapper" style={{display: 'inline-flex'}}>
                <a href={ISSUES_URL}>
                  <img className="issuesLogo" src={ISSUES_SVG} alt='Issues'></img>
                </a>
                <p className="stats">{this.state.issues[5]}</p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="statsWrapper" style={{display: 'inline-flex'}}>
                <img className="testsLogo" src={TESTS_SVG} alt='Tests'></img>
                <p className="stats">52</p>
              </div>
            </div>
          </div>
          <div style={{display: 'flex', "flex-direction": "column"}}>
            <div style={{width: "100%"}}>
              {/* Data Sources */}
              <h1 id="letter-spacing" style={{paddingBottom: '35px', paddingTop: '35px'}}>DATA</h1>
              {/* <div className="container" style={{display: 'grid'}}> */}
                {/* WHO */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={WHO_API}>
                    <img className="gitlabLogo" src='https://upload.wikimedia.org/wikipedia/commons/9/9d/WHO.svg' alt='WHO'></img>
                  </a>
                  <span class="tooltiptext"><p>WHO Athena API: Scraped using <br />python query via requests and JSON package</p></span>
                  <p className="stats">World Health Organization API</p>
                </div>

                {/* GHR */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={GBD_DATA}>
                    <img className="issuesLogo" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/
                      2wCEAAkGBxMSEhUTEhIVFRUXFxgVFRUWFxcXFRUVFRcdFxcVFRUYHSggHR0lHRcYITEhJSkrLi4u
                      GB8zODMtNygtLisBCgoKDg0OGhAQGzclIB4tLS0rLS0tLSs3KystKzg1LSstLS0tLS0tLSsuLTct
                      LS0tLS0tLS0tLSstLS0tLS0tLf/AABEIAJEAmgMBIgACEQEDEQH/xAAbAAADAAMBAQAAAAAAAAAA
                      AAAAAQIDBAUGB//EADQQAAIBAgMFBgYCAQUAAAAAAAABAgMRITHwBAVBUYESYXGRocEGEyIysdEU
                      QlJTgpLh8f/EABkBAQADAQEAAAAAAAAAAAAAAAACAwQBBf/EACQRAQACAgEEAgIDAAAAAAAAAAAB
                      AgMRBBIhMUEyURNhBRQV/9oADAMBAAIRAxEAPwD7UAAAxAMCbYjcRcehWteQEg5FABjX3FkpYsdt
                      MBcWU9a1xIvZ5eRVwFSy13Da5CpZa7iwMbkOqr4BUQpYWvzAtD1r0IpPAvWvUCKrwZaZjrfa9a4m
                      RaxAQAGteYDABADYE1JWxNWttL4EL3ivk22Z1Us2YJbXyRqiM1s9p8I7Zv5Er3H/ACZc/QwAQ/Jb
                      7NthbU+JlW0ReeBpASjNaDbo0nhzMikc2nUayN2lVUvHX6NFMsW7OxLJP3JqcPEJr8iqSxXj7Frp
                      wXZw4FpkxWL6fhg42y8gKkrkfM7mWpXNL5rA3dfgAC4DFJ2BM09qq8CF7dMbEbRW7T7jEIDDMzM7
                      lAAAHAAAAAAAAOLtkIAN6lW7S7zJVzXic+ErM31K/ZesjZiydUalKJU1Z4DUta6iksV1KZc6lx5Y
                      Gu4eBs2fA13J8n5gbIIBgTUlZXOVUqpfc0vFm9tkrK3P2PA7/wBtl86SUnaOGbtzyMXJvqdKsuTo
                      jb1v8iH+cfNFRqJ5NPqeEp7fL+2PgbtHa2/tk/Nmb8imOREvYAecob0nHN3Xfizr7JvGM8MmSi0S
                      srkiW4AASWAAAAAAADY2WfDqa5dN2ZPHbVol2HQqPAsmWQ4m9IyewuRQWQCHrXoJggNTeDwXU+Y7
                      XW7VSd874+R9O3hkup8m22XYryvzs/JYnm8r5MPMtrTOCdsUCYGVlb+zbR2sHn+TZTOOmdDZa/aV
                      nmiUSsrZ3t27za+meXB6R2kzxx2N0bd/ST8PRWLK29S048nqXZAALGgAAAA0IAOosugQyQ1kKB6S
                      agt3gKwAwTBijkBg22OF+R8s+K9n7O0SfCWK8kj6zUjdNHiPjXd7nBVEsY4W7sW2YeXT2x83H1Y+
                      3p47Zdot9L6P2N05LRlo7Q44Zr8GB5ePL6l0RxbTuszXhtUWX8+PM6v6o+3XoVlJflGWLs7o4Udu
                      UXdP0NyG81JfSvUltOuSJ9va7s2rtx70bh4GjvGpB3jK3NWX6Og9oclnfyLIu1Vzxp6z5i5lHj7m
                      WhtMoP6X+DvWlGf9PVjic7d28fmYNYnXowwbLccdVoXVmJ7w3kKLxY45EvPoegsWHmAgBijkNijk
                      BRz95bKpJp5STT6qx0Na8iZRurEMlOqNOTG3x/fu7JUKjVvpbbi+Fm3ZZZ2RzT6tvzdEasXGXR8j
                      5vvXdk6EuzJYcHhjl3955OTHNZeJyuNOOdx4aIABWxgcW1isGIA7E6bdPbP8l5ZG5sm8Yxwbsu/g
                      cgLDa6ue0PSrbI8HfwD+ZHkzzlO9/peJ7X4b3JJWqVkr/wBY4d+dnjwJ13LVhvOWdRDsbm2PspNq
                      0nzzR3nHsxt+OZFGhZXeZsVMmengx9MberWvTGgnhrXIUs0+hViKiy8S9JkF0JV/Eq75eqAGKOQ2
                      KOQFCGAEyinmczeG74zXZku0nwx9jqkSWK6ld8cX8uTET2l863p8IyV3Sd1n2XhbwPO7Ru+rB2lC
                      XjZ287H2WdCL/wCjVr7Anmk134mK/Fn0w5OBS3evZ8basI+rz3HSbxoU3/sX6JXw/R/0Kf8Awj+i
                      r+vdn/z7/b5dCjJ5Rk/BNnV3f8OVqn9eyuN8H5M+i0d0QX204R8Fb2NxbKkncnXi2nytp/HxHyl5
                      7cnw5CjZpdqf+TurZYZ24HpKFDs4vMugvpXgZDbjwxRvpjrSNVhMxyFP3KLk0w16BPL19AhlrmNr
                      AAi8ENMmGRXQBMUchsUcgKAA1rXIAJlzK1rXIx1cta0gMmta4Ezy1r/wbkTKQDjn5exS16GOLxfQ
                      pSAomeT8Ne47ilkwJoP6V5GQx0FgZAJmUTMoDHRyMhjoZef5MgGOlx8TJriQni/ApgD16ihl0/YA
                      BT16g9eoABNTJ65mKpkuoABlp5LXIrgAAJZvXEbyGAGu89dxVPJ64MAAdHLXeZHr1AAJmUteowAx
                      Uctd5kevUAAxr7n4fspgAH2Q"
                    />
                  </a>
                  <span class="tooltiptext"><p>GBD Data Tool: Scraped using <br />python via csv package</p></span>
                  <p className="stats">GHRx API</p>
                </div>
                {/* World Bank */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={WORLD_BANK}>
                    <img className="testsLogo" src="https://seeklogo.com/images/W/world-bank-logo-50FE63E4E3-seeklogo.com.png" alt='Tests' />
                  </a>
                  <span class="tooltiptext"><p>World Bank API : Scraped using <br />python query via requests package</p></span>
                  <p className="stats">The World Bank API</p>
                </div>
              {/* </div> */}
            </div>

            <div style={{width: "100%"}}>
              {/* Tools */}
              <h1 id="letter-spacing" style={{paddingBottom: '35px', paddingTop: '35px'}}>TOOLS</h1>
              {/* <div className="container" style={{display: 'grid'}}> */}
                {/* React */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={REACT_LINK}>
                    <img className="gitlabLogo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1024px-React-icon.svg.png" alt='GitLab'></img>
                  </a>
                  <span class="tooltiptext"><p>We used REACT to build the <br />UI for our frontend</p></span>
                  <p className="stats">React</p>
                </div>

                {/* Bootstrap */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={BOOTSTRAP_LINK}>
                    <img className="issuesLogo" src="https://i.ya-webdesign.com/images/bootstrap-svg-3.png" alt='Issues'></img>
                  </a>
                  <span class="tooltiptext"><p>We used Bootstrap to build <br />our frontend templates and CSS</p></span>
                  <p className="stats">Bootstrap</p>
                </div>

                {/* Django */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={DJANGO_LINK}>
                  <img className="testsLogo" src="https://static.djangoproject.com/img/logos/django-logo-positive.svg" alt='Tests'></img>
                  </a>
                  <span class="tooltiptext"><p>We used Django to build a python-based <br />framework for our backend</p></span>
                  <p className="stats">Django</p>
                </div>

                {/* Django REST  */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={DJANGO_REST_LINK}>
                    <img className="testsLogo" src="https://miro.medium.com/max/1400/1*MQ-Lf8tmtfa-pumN2Sh0cw.png" alt='Tests'></img>
                  </a>
                  <span class="tooltiptext"><p>We used Django Rest Framework to build <br />the REST API for our backend</p></span>
                  <p className="stats">Django REST Framework</p>
                </div>

                {/* PostgreSQL */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={POSTGRES_LINK}>
                    <img className="testsLogo" src="https://upload.wikimedia.org/wikipedia/commons/2/29/Postgresql_elephant.svg" alt='Tests'></img>
                  </a>
                  <span class="tooltiptext"><p>We used PostgreSQL as our <br />database which we connected to Amazon RDS</p></span>
                  <p className="stats">PostgreSQL</p>
                </div>

                {/* AWS */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={AWS_LINK}>
                    <img className="testsLogo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Amazon_Web_Services_Logo.svg/1280px-Amazon_Web_Services_Logo.svg.png" alt='Tests'></img>
                  </a>
                  <span class="tooltiptext"><p>We used AWS to host <br />our website-related resources</p></span>
                  <p className="stats">AWS</p>
                </div>

                {/* Gitlab */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={GITLAB_LINK}>
                    <img className="testsLogo" src={GITLAB_SVG} alt='Tests'></img>
                  </a>
                  <span class="tooltiptext"><p>We used GitLab as a <br />repository for our codebase</p></span>
                  <p className="stats">GitLab</p>
                </div>

                {/* Selenium */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={SELENIUM_LINK}>
                    <img className="testsLogo" src="https://cdn-images-1.medium.com/max/228/1*DhdHDzruFyR4kN417zxiVA.png" alt='Tests'></img>
                  </a>
                  <span class="tooltiptext"><p>We used Selenium alongside <br />Python to run our frontend unit tests</p></span>
                  <p className="stats">Selenium</p>
                </div>

                {/* Mocha */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={MOCHA_LINK}>
                    <img className="testsLogo" src="https://miro.medium.com/max/525/1*UlY-YcU41lTbgztaEMResw.jpeg" alt='Tests'></img>
                  </a>
                  <span class="tooltiptext"><p>We used Mocha to test <br />our frontend JS code</p></span>
                  <p className="stats">Mocha</p>
                </div>

                {/* Postman */}
                <div className="statsWrapper" style={{display: 'inline-flex'}}>
                  <a href={POSTMAN_LINK}>
                    <img className="testsLogo" src="https://2jws2s3y97dy39441y2lgm98-wpengine.netdna-ssl.com/wp-content/uploads/2020/10/postman.jpg" alt='Tests'></img>
                  </a>
                  <span class="tooltiptext"><p>We used Postman to perform <br />integration tests for our backend</p></span>
                  <p className="stats">Postman</p>
                </div>
              {/* </div> */}
            </div>
          </div>
          <a href="https://gitlab.com/Tarun-Prince99/pathogerm" class="btn btn-info" role="button">GitLab Repository</a>
          <span> </span>
          <a href="https://documenter.getpostman.com/view/10491970/SzKWuxRU" class="btn btn-info" role="button">Postman RESTful API</a>
          <span> </span>
          <a href="https://youtu.be/ecWDRMu8U54" class="btn btn-info" role="button">Presentation</a>
        </div>
      </div>
    )
  }

  // getCommits(parsedJson) {
  //   console.log(parsedJson);
  //   var users = [0, 0, 0, 0, 0, 0]
  //   parsedJson.forEach(function (commit) {
  //     if (commit.committer_name === 'Brian LeQuang') {
  //       users[0]++;
  //     } else if (commit.committer_name === 'Michael Geng') {
  //       users[1]++;
  //     } else if (commit.committer_name === "Tarun Prince") {
  //       users[2]++;
  //     } else if (commit.committer_name === "Darren Phua") {
  //       users[3]++;
  //     } else if (commit.committer_name === "Paul Sathuluri") {
  //       users[4]++;
  //     }
  //     users[5]++;
  //   })
  //   this.setState({users: users})
  // }

  getCommits(parsedJson) {
    console.log(parsedJson);
    var users = [0, 0, 0, 0, 0, 0]
    parsedJson.forEach(function (person) {
      if (person.name === 'Brian LeQuang') {
        users[0] += person.commits;
      } else if (person.name === 'Michael Geng' || person.name === 'mjgeng') {
        users[1] += person.commits;
      } else if (person.name === "Tarun Prince") {
        users[2] += person.commits;
      } else if (person.name === "Darren Phua") {
        users[3] += person.commits;
      } else if (person.name === "Paul Sathuluri") {
        users[4] += person.commits;
      }
      users[5] += person.commits;
    })
    this.setState({users: users})
  }

  getIssues(parsedJson) {
    var issues = [0, 0, 0, 0, 0, 0]
    parsedJson.forEach(function (issue) {
      if (issue.author.name === 'Brian LeQuang') {
        issues[0]++;
      } else if (issue.author.name === 'Michael Geng' || issue.author.name === 'mjgeng') {
        issues[1]++;
      } else if (issue.author.name === "Tarun Prince") {
        issues[2]++;
      } else if (issue.author.name === "Darren Phua") {
        issues[3]++;
      } else if (issue.author.name === "Paul Sathuluri") {
        issues[4]++;
      }
      if (issue.closed_by != null) {
        if (issue.closed_by.name === 'Brian LeQuang') {
          issues[0]++;
        } else if (issue.closed_by.name === 'Michael Geng'|| issue.closed_by.name === 'mjgeng') {
          issues[1]++;
        } else if (issue.closed_by.name === "Tarun Prince") {
          issues[2]++;
        } else if (issue.closed_by.name === "Darren Phua") {
          issues[3]++;
        } else if (issue.closed_by.name === "Paul Sathuluri") {
          issues[4]++;
        }
      }
      issues[5]++;
    })
    console.log(parsedJson)
    this.setState({issues: issues})
  }

  getData() {
    console.log("Started parsing")
    // fetch(GITLAB_API_URL + '/repository/commits?per_page=1000')
    //   .then(res => res.json())
    //   .then(parsedJson => this.getCommits(parsedJson));
    fetch(GITLAB_API_URL + '/repository/contributors')
      .then(res => res.json())
      .then(parsedJson => this.getCommits(parsedJson));
    fetch(GITLAB_API_URL + '/issues?per_page=1000')
      .then(res => res.json())
      .then(parsedJson => this.getIssues(parsedJson))
  }
}
