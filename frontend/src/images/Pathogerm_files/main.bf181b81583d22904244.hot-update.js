webpackHotUpdate("main",{

/***/ "./src/views/Map.js":
/*!**************************!*\
  !*** ./src/views/Map.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Map; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_tooltip__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-tooltip */ "./node_modules/react-tooltip/dist/index.es.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_MapChart_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/MapChart.js */ "./src/components/MapChart.js");
/* harmony import */ var _css_Page_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../css/Page.css */ "./src/css/Page.css");
/* harmony import */ var _css_Page_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_css_Page_css__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/u/brianleq/swe/pathogerm/frontend/src/views/Map.js";





class Map extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  // componentDidMount() {
  //   this.mapState();
  // }
  constructor(props) {
    super(props);
    this.state = {
      content: "",
      setContent: ""
    };
  }

  componentDidMount() {
    this.mapContent();
  }

  mapContent() {
    this.state.content = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
    this.state.setContent = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  }

  render() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "dark-back",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_MapChart_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      setTooltipContent: this.state.setContent,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_tooltip__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, this.state.content), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_MapChart_js__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }));
  }

}

/***/ })

})
//# sourceMappingURL=main.bf181b81583d22904244.hot-update.js.map