import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Landing from "./views/Landing.js";
import Diseases from "./views/Diseases.js";
import Map from "./views/Map.js";
import Timeline from "./views/Timeline.js";
import Information from "./views/Information.js";
import About from "./views/About.js";
import Navbar from "./components/Navbar.js";
import Search from "./views/Search.js"
import Coronavirus from "./views/Coronavirus.js"
import TimelineList from "./views/TimelineList.js"
import Visualizations from "./views/Visualizations.js"

function App() {
  return (
    <div>
      <Router>
        <Navbar/>
        <Switch>
          <Route exact path="/" component={Landing} />
          <Route exact path="/diseases" component={Diseases} />
          <Route exact path="/map" component={Map} />
          <Route path="/map/:id" component={Map} />
          <Route exact path="/timeline" component={TimelineList} />
          <Route path="/timeline/:id" component={Timeline} />
          <Route path="/coronavirus" component={Coronavirus} />
          <Route path="/information/:id" component={Information} />
          <Route exact path="/about" component={About} />
          <Route path="/search/:id" component={Search} />
          <Route path="/visualizations" component={Visualizations} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
