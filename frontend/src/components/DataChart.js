import React from "react";
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import "../css/Text.css"

const DataChart = (title, key, stats) => {
  return (
    <div className="row" style={{padding: "30px", display: "inline-block"}}>
   		<BarChart
        width={1000}
        height={600}
        padding={30}
        data={stats}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
        style = {{
          color:"#fff"
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip contentStyle={{ backgroundColor: '#000000' }} />
        <Legend />
        <Bar name={title} dataKey={key} fill="#DD2C06"/>
      </BarChart>
    </div>
  )
}

export {DataChart};