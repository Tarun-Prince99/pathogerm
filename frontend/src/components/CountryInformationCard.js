import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";

export default function CountryInformationCard(props) {
  const data = props.data

  return (
    <Link to={`timeline/${data.year}`}>
      <Card>
        <CardContent>
          {renderAttribute("", data.year)}
          {renderAttribute("Physicians per 10,000 People", data.med_doctors)}
          {renderAttribute("Infectious Diseases Percentage", data.uhc_infect)}
          {renderAttribute("Non-Communicable Diseases Prevalence", data.uhc_noncom)}
          {renderAttribute("Life Expectancy ", data.life_expect)}
          {renderAttribute("Government Health Budget(USD)", data.health_budg)}
        </CardContent>
      </Card>
    </Link>
    
  );
}

function renderAttribute(description, data) {
  if (data == null) {
    data = "na"
  }
  if (description != "") {
    description += " : "
  }
  return <Typography variant="h5" component="h2">
      {description}{data}
    </Typography>;
  
}