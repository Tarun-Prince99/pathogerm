import React from "react";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import MaterialTable from 'material-table'
import API_PATH from "../components/ApiPath.js";
import isoCountries from "../components/CountryMappings.js";
import Highlighter from "react-highlighter"

import "../css/Page.css"
import "../css/Text.css"

const whiteColor = "#FFF"
const redColor = "#DD2C06"
const darkColor = "#202020"
const theme = createMuiTheme({
  overrides: {
    MuiPaper: {
      root: {
        backgroundColor: darkColor,
        color: whiteColor
      },
      elevation2: {
        boxShadow: "none"
      }
    },
    MuiTableSortLabel: {
      root: {
        "&:hover": {
          color: redColor,
          '&& $icon': {
            opacity: 1,
            color: redColor
          },
        },
        "&$active": {
          color: redColor,
          '&& $icon': {
            opacity: 1,
            color: redColor
          },
        },
      }
    },
    MuiInputBase: {
      root: {
        backgroundColor: "inherit",
        color: whiteColor
      },
      input: {
        color: whiteColor
      }
    },
    MuiInput: {
      underline: {
        "&:before": {
          borderBottom: `1px solid ${whiteColor}`
        },
        "&:after": {
          borderBottom: `1px solid ${redColor}`
        },
        "&:hover": {
          borderBottom: `1px solid ${redColor}`
        }
      }
    },
    MuiTableCell: {
      body: {
        backgroundColor: darkColor,
        color: whiteColor
      }
    },
    MuiIconButton: {
      root: {
        color: whiteColor
      }
    },
    MuiCircularProgress: {
      colorPrimary: {
        color: darkColor
      }
    },
    MuiTypography: {
      caption: {
        color: whiteColor
      }
    }
  }
});

const StatsTable = (title, header, pageSize, path, map, queryColumn, fullCount) => {
  return (
    <MuiThemeProvider theme={theme} >
      <MaterialTable
        title={title}
        columns={header}
        data={query =>
          new Promise((resolve, reject) => {
            let url = API_PATH + path + "&limit=" + fullCount + "&search=" + query.search + "&offset=" + pageSize*(query.page)
            console.log(url)
            fetch(url).then(response => response.json()).then(result => {
              result.results.map(function(obj){
                var masterData = []
                for (var i = 0; i < queryColumn.length; i++){
                  masterData.push("");
                }
                var queryList = query.search.split(" ")
                for(var i = 0; i < queryList.length; i++) {
                  for(var qi = 0; qi < queryColumn.length; qi++) {
                    let searchVal = Number.isInteger(obj[queryColumn[qi]]) ? obj[queryColumn[qi]].toString() : obj[queryColumn[qi]].toLowerCase();
                    let searchRet = Number.isInteger(queryList[i]) ? queryList[i].toString() : queryList[i].toLowerCase();
                    if(searchVal.includes(searchRet)){
                      console.log("adding " + searchRet)
                      masterData[qi] += (searchRet + " ")
                      break
                    }
                  }
                }
                for (var qi = 0; qi < queryColumn.length; qi++){
                  obj[queryColumn[qi]] = <Highlighter search = {masterData[qi].toString().trim()}>{obj[queryColumn[qi]].toString()}</Highlighter>
                }
                return obj
              })
              resolve({
                // data: result.results.filter(x => x[queryColumn].toLowerCase().some(query.search.toLowerCase().split(" "))),
                data: result.results,
                page: query.page,
                totalCount: result.count,
              });
              console.log(result.results)
            })
          })
        }
        options={{
          headerStyle: { backgroundColor: darkColor, color: whiteColor },
          rowStyle: { backgroundColor: darkColor, color: whiteColor },
          pageSize: pageSize,
          pageSizeOptions: [5, 10, 15],
          sorting: true
        }}
        actions={map ? [
          {
            icon: "map",
            tooltip: "View Map",
            onClick: (event, rowData) => window.location.href="/map/" + rowData.cause.props.children
          },
          {
            icon: "info",
            tooltip: "More Information",
            onClick: (event, rowData) => window.location.href="/information/" + rowData.cause.props.children
          }
        ] : title === "Year Data" ?
          [
            {
              icon: "launch",
              tooltip: "Show Attributes",
              onClick: (event, rowData) => window.location.href="/timeline/" + rowData.year.props.children
            }
          ]
        : [
          {
            icon: "info",
            tooltip: "More Information",
            onClick: (event, rowData) => window.location.href="/information/" + ((rowData.name!==undefined) ? rowData.name.props.children : isoCountries[rowData.location.props.children])
          }
        ]}
      />
    </MuiThemeProvider>
  )
}

const StaticTable = (title, header, stats, pageSize, pageSizeOptions, action) => {
  return (
    <MuiThemeProvider theme={theme}>
      <MaterialTable
        title={title}
        columns={header}
        data={stats}
        options={{
          headerStyle: { backgroundColor: darkColor, color: whiteColor },
          rowStyle: { backgroundColor: darkColor, color: whiteColor },
          pageSize: pageSize,
          pageSizeOptions: pageSizeOptions,
          search: false,
          sorting: false
        }}
        actions={action ? [
          {
            icon: "info",
            tooltip: "More Information",
            onClick: (event, rowData) => window.location.href="/information/" + rowData.name
          }
        ] : []}
      />
    </MuiThemeProvider>
  )
}

export {StatsTable, StaticTable}