import React from "react";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import "../css/Page.css"
import "../css/Text.css"

const DiseaseTable = ({ header, stats }) => (
  <div>
    <Table aria-label="simple table">
      <TableHead>
        <TableRow>
          {header.map(title => (
            <TableCell key={ title } className="white">{title.id}</TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {stats.map(stat => (
          <TableRow key={ stat }>
            <TableCell className="white" component="th" scope="row">
              { stat.id }
            </TableCell>
            <TableCell className="white">{stat.number}</TableCell>
            <TableCell className="white">{stat.rate}</TableCell>
            <TableCell className="white">{stat.percent}</TableCell>
            <TableCell className="white">{stat.sex}</TableCell>
            <TableCell className="white">{stat.age}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </div>
)

export default DiseaseTable;