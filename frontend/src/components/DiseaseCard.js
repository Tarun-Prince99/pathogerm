import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';


export default function DiseaseCard(props) {
  const data = props.data
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="h2">
          {data.title}
        </Typography>
        <Typography variant="h5" component="h2">
          {data.number}
        </Typography>
        <Typography variant="h5" component="h2">
          {data.country}
        </Typography>
        <Typography variant="h5" component="h2" href={"/timeline/" + data.year}>
          {data.year}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" href={"/timeline/" + data.year}>
        {data.year} Information
        </Button>
        <Button size="small" color="primary" href={"/information/" + data.country}>
        {data.country} Information
        </Button>
      </CardActions>
    </Card>
  );
}
