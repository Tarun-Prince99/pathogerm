import React from 'react';
import { VerticalTimelineElement }  from 'react-vertical-timeline-component';

import 'react-vertical-timeline-component/style.min.css';
import '../css/Text.css';
import '../css/Page.css';

const TimelineNode = ({ time, desc }) => (
  <VerticalTimelineElement
    className="vertical-timeline-element--work"
    contentArrowStyle={{ borderRight: '10px solid  #D2D6DA' }}
    contentStyle={{background: '#D2D6DA'}}
    iconStyle={{ background: '#DD2C06', color: '#fff' }}
    icon={'.ico'}
  >
    <h3 className="vertical-timeline-element-title">{desc}</h3>
    <p>
      {time}
    </p>
  </VerticalTimelineElement>
);

export default TimelineNode;
