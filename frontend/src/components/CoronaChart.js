import React, { memo } from "react";
import hardCodeMap from '../components/hardCodeMap.json';
import {
  ZoomableGroup,
  ComposableMap,
  Geographies,
  Geography,
  Sphere,
  Graticule
} from "react-simple-maps";
import { scaleLinear } from "d3-scale";

const geoUrl = hardCodeMap

const percent = num => {
  return (num / 1000000) * 100
};

const MapChart = ({ setTooltipContent, setCountry, setISO, setSlug, data }) => {
  var results = data !== null ? data.Countries : []
  var max_rate = 0
  for(var i = 0; i < results.length; ++i) {
    max_rate = Math.max(max_rate, results[i].TotalConfirmed)
  }
  console.log(max_rate)
  max_rate *= .1  
  var colorScale = scaleLinear()
    .domain([0, max_rate])
    .range(["#D6D6DA", "#ff5233"]);
  return (
    <>
      <ComposableMap data-tip="" projectionConfig={{ scale: 210 }}>
        <ZoomableGroup>
          <Sphere stroke="#E4E5E6" strokeWidth={0.5} />
          <Graticule stroke="#E4E5E6" strokeWidth={0.1} />
          <Geographies geography={geoUrl}>
            {({ geographies }) =>
              geographies.map(geo => {
                const d = results.find(s => s.CountryCode === geo.properties.ISO_A2)
                return (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  fill={d ? colorScale(d.TotalConfirmed) : "#D6D6DA"}
                  onMouseEnter={() => {
                    const { NAME } = geo.properties;
                    var rate = d ? d.TotalConfirmed : 0;
                    setTooltipContent(`${NAME} - ` + rate + " total cases");
                  }}
                  onClick={() => {
                    const {NAME, ISO_A2} = geo.properties;
                    setCountry(`${NAME}`);
                    setSlug(d.Slug)
                    setISO(`${ISO_A2}`);
                  }}
                  onMouseLeave={() => {
                    setTooltipContent("");
                  }}
                  style= {{
                    default: {
                      outline: "none"
                    },
                    hover: {
                      fill: "#606060",
                      outline: "none"
                    },
                    pressed: {
                      fill: "#808080",
                      outline: "none"
                    }
                  }}
                />
              )})
            }
          </Geographies>
        </ZoomableGroup>
      </ComposableMap>
    </>
  );
};

export default memo(MapChart);
