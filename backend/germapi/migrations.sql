\copy germapis_disease (cause, measure, year, location, number, percent, rate) FROM '/Users/darrenphua/Desktop/Spring2020/CS373/pathogerm/parsing/database_GDB_data.csv' DELIMITER ',' CSV HEADER;

\copy germapis_country (iso, name, pop, gdp, mle, fle, cbr, imr, com, noncom, md, inf_d, hexp, incomelevel) FROM '/Users/darrenphua/Desktop/Spring2020/CS373/pathogerm/parsing/database_country_info.csv' WITH (DELIMITER ',', FORMAT CSV, HEADER, FORCE_NOT_NULL(incomelevel));

\copy germapis_year (year, name, med_doctors, uhc_infect, uhc_noncom, deaths_noncom, life_expect, est_preval_dep, health_per_gdp, health_budg, income_levl) FROM '/Users/darrenphua/Desktop/Spring2020/CS373/pathogerm/parsing/database_whocsv.csv' WITH (DELIMITER ',', FORMAT CSV, HEADER, FORCE_NOT_NULL(income_levl));

ALTER TABLE germapis_year DROP CONSTRAINT germapis_year_pkey;
ALTER TABLE germapis_year ADD PRIMARY KEY (year, name);

ALTER TABLE germapis_disease DROP CONSTRAINT germapis_disease_pkey;
ALTER TABLE germapis_disease ADD PRIMARY KEY (cause_name, measure_name, year, location_name);

ALTER TABLE germapis_disease
ADD CONSTRAINT germapis_disease_fk_year
FOREIGN KEY (year, location_name)
REFERENCES germapis_year(year, name)
ON DELETE CASCADE;

DROP TABLE germapis_disease, germapis_year, germapis_country