from germapis.models import Disease, Year, Country
from rest_framework import serializers


class DiseaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Disease
        fields = [
            "cause",
            "measure",
            "year",
            "location",
            "description",
            "number",
            "percent",
            "rate",
        ]


class YearSerializer(serializers.ModelSerializer):
    class Meta:
        model = Year
        fields = [
            "year",
            "name",
            "med_doctors",
            "uhc_infect",
            "uhc_noncom",
            "deaths_noncom",
            "life_expect",
            "est_preval_dep",
            "health_per_gdp",
            "health_budg",
            "income_levl",
        ]


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = [
            "iso",
            "name",
            "pop",
            "gdp",
            "mle",
            "fle",
            "cbr",
            "imr",
            "com",
            "noncom",
            "md",
            "inf_d",
            "hexp",
            "incomelevel",
        ]
