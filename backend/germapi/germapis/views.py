from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, Http404
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import generics
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from germapis import models
from germapis import serializers
from germapis.filters import SearchFilterAllValues

# Create your views here.


def index(request):
    return HttpResponse("Hello World! Welcome to Pathogerm backend.")


class DiseaseViewset(viewsets.ReadOnlyModelViewSet):
    queryset = models.Disease.objects.all().order_by(
        "cause", "measure", "year", "location"
    )
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if('global' in request._request.GET) :
            if request._request.GET['global'] == 'false':
                queryset = queryset.exclude(location='GLO')
        if('all_causes' in request._request.GET) :
            if request._request.GET['all_causes'] == 'false':
                queryset = queryset.filter(
                    ~Q(cause='All causes') & ~Q(cause='Non-communicable diseases') & ~Q(cause='Other non-communicable diseases') & ~Q(cause='Communicable, maternal, neonatal, and nutritional diseases'))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    
    serializer_class = serializers.DiseaseSerializer
    filter_backends = [DjangoFilterBackend, SearchFilterAllValues, filters.OrderingFilter]
    filterset_fields = ["cause", "measure", "year", "location"]
    ordering_fields = '__all__'
    search_fields = [
        "cause",
        "measure",
        "location__iso",
    ]


class YearViewset(viewsets.ReadOnlyModelViewSet):
    queryset = models.Year.objects.all().order_by("year", "name")
    serializer_class = serializers.YearSerializer
    filter_backends = [DjangoFilterBackend, SearchFilterAllValues, filters.OrderingFilter]
    filterset_fields = ["year", "name"]
    ordering_fields = '__all__'
    search_fields = [
        "year",
        "name__iso",
        "income_levl",
    ]


class CountryViewset(viewsets.ReadOnlyModelViewSet):
    queryset = models.Country.objects.all().order_by("iso")
    serializer_class = serializers.CountrySerializer
    filter_backends = [DjangoFilterBackend, SearchFilterAllValues, filters.OrderingFilter]
    filterset_fields = ["iso"]
    ordering_fields = '__all__'
    search_fields = [
        "iso",
        "name",
        "incomelevel",
    ]

class DiseaseViewsetWithoutGlobal(viewsets.ReadOnlyModelViewSet):
    queryset = models.Disease.objects.all().order_by(
        "cause", "measure", "year", "location"
    )
    serializer_class = serializers.DiseaseSerializer
    filter_backends = [DjangoFilterBackend, SearchFilterAllValues, filters.OrderingFilter]
    filterset_fields = ["cause", "measure", "year", "location"]
    ordering_fields = '__all__'
    search_fields = [
        "cause",
        "measure",
        "location__iso",
    ]