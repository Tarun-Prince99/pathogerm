from django.db import models

# Create your models here.
class Country(models.Model):
    iso = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=100)
    pop = models.FloatField(null=True)
    gdp = models.FloatField(null=True)
    mle = models.FloatField(null=True)
    fle = models.FloatField(null=True)
    cbr = models.FloatField(null=True)
    imr = models.FloatField(null=True)
    com = models.FloatField(null=True)
    noncom = models.FloatField(null=True)
    md = models.FloatField(null=True)
    inf_d = models.FloatField(null=True)
    hexp = models.FloatField(null=True)
    incomelevel = models.CharField(max_length=10, default="")

    def __str__(self) :
        return str(self.iso) + str(self.name)


class Year(models.Model):
    class Meta:
        unique_together = (("year", "name"),)

    year = models.IntegerField(primary_key=True)
    name = models.ForeignKey(Country, on_delete=models.CASCADE, db_column="name")
    med_doctors = models.FloatField(null=True)
    uhc_infect = models.FloatField(null=True)
    uhc_noncom = models.FloatField(null=True)
    deaths_noncom = models.FloatField(null=True)
    life_expect = models.FloatField(null=True)
    est_preval_dep = models.FloatField(null=True)
    health_per_gdp = models.FloatField(null=True)
    health_budg = models.FloatField(null=True)
    income_levl = models.CharField(max_length=20)
    
    def __str__(self) :
        return str(self.year) + str(self.name)


class Disease(models.Model):
    class Meta:
        unique_together = (("cause", "measure", "year", "location"),)

    cause = models.CharField(max_length=100, primary_key=True)
    measure = models.CharField(max_length=50)
    description = models.TextField(default="")
    year = models.IntegerField()
    location = models.ForeignKey(
        Country, on_delete=models.CASCADE, db_column="location"
    )
    number = models.FloatField(null=True)
    percent = models.FloatField(null=True)
    rate = models.FloatField(null=True)

    def __str__(self) :
        return str(self.cause) + str(self.measure) + str(self.year) + str(self.location)
