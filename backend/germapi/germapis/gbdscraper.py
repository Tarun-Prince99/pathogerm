import sys
import csv
import json
import requests
import csv
import pandas as pd
import numpy as np


class Disease:
    def __init__(self, location, name, year, value):
        self.loc = location
        self.name = name
        self.year = year
        self.value = value

    def __str__(self):
        return (
            self.name
            + " at "
            + self.loc
            + " in "
            + self.year
            + " with value "
            + self.value
        )

    def returnData(self):
        fullData = []
        fullData.append(self.loc)
        fullData.append(self.name)
        fullData.append(self.year)
        fullData.append(self.value)
        return fullData


def build_disease_list():
    diseaseList = []
    with open("./gbd/IHME-GBD_2017_DATA-b6d440b9-1.csv", "r") as file:
        reader = csv.reader(file)
        for row in reader:
            diseaseList.append(
                Disease(str(row[3]), str(row[9]), str(row[12]), str(row[13]))
            )

    # for disease in diseaseList[1:]:
    # print(diseaseList[1].returnData())
    return diseaseList


class Data_Entry:
    def __init__(self, name, full_name, value, type, year):
        self.name = name
        self.fn = full_name
        self.val = value
        self.type = type
        self.year = year

    def __str__(self):
        return (
            self.fn
            + "("
            + self.name
            + "): "
            + self.val
            + " of label "
            + self.type
            + " in "
            + self.year
        )

    def name(self):
        return self.fn

    def type(self):
        return self.type

    def __iter__(self):
        yield self.name
        yield self.fn
        yield self.val
        yield self.type
        yield self.year


urltemplate = "http://apps.who.int/gho/athena/api/GHO{mylabel}/?format=json"
url = urltemplate.format(mylabel="")
# data = urllib.request.urlopen(url)

data = requests.get(url).json()

# pprint(data['dimension'])
dim = data["dimension"][0]
code_list = dim["code"]

country_list = {}
label_list = []
desc_list = []
i = 0
# obtain list of labels
for val in code_list:
    label_list.append(val["label"])
    desc_list.append(val["display"])

final_label_list = {}
final_label_list[label_list[3191]] = desc_list[3191]
final_label_list[label_list[3277]] = desc_list[3277]
final_label_list[label_list[3278]] = desc_list[3278]
final_label_list[label_list[2770]] = desc_list[2770]
final_label_list[label_list[56]] = desc_list[56]
final_label_list[label_list[2765]] = desc_list[2765]
final_label_list[label_list[2733]] = desc_list[2733]
final_label_list[label_list[2487]] = desc_list[2487]  # No entries
final_label_list[label_list[2160]] = desc_list[2160]  # No entries
final_label_list[label_list[255]] = desc_list[255]
final_label_list[label_list[428]] = desc_list[428]

# for key in final_label_list:
# 	print(key,' : ', final_label_list[key])

# pprint(page_data['fact'][5])
country_url = "http://apps.who.int/gho/athena/api/COUNTRY?format=json"
country_page = requests.get(country_url).json()

# populate country list with key-value pairing code with country name
for items in country_page["dimension"][0]["code"]:
    country_list[items["label"]] = items["display"]
country_list["NOT FOUND"] = "NOT FOUND"

data_list = []
# print(final_label_list.keys())
for keys in final_label_list.keys():
    urlend = "/" + keys
    newurl = urltemplate.format(mylabel=urlend)
    page_data = requests.get(newurl).json()
    # 	pprint(len(page_data['fact']))
    for item in page_data["fact"]:
        name = "NOT FOUND"
        year = -1
        for subitem in item["Dim"]:
            if subitem["category"] == "COUNTRY":
                name = subitem["code"]
            elif subitem["category"] == "YEAR":
                year = subitem["code"]
        # print('name: ', name, ', year: ', year, 'val: ', item['value']['display'], ', type: ', final_label_list[keys])
        data_list.append(
            Data_Entry(
                name,
                country_list[name],
                item["value"]["display"],
                final_label_list[keys],
                year,
            )
        )

rows = {}
start_header = ["name", "full_name", "year"]
header = []
# number of attributes in list is hardcoded to 11
for entry in data_list:
    key = (entry.name, entry.fn, entry.year)
    if key not in rows:
        rows[key] = [None] * 9
    if entry.type not in header:
        header.append(entry.type)
    rows[key][header.index(entry.type)] = entry.val

rowslist = []
for key in rows:
    rowslist.append(list(key) + rows.get(key))

replacements = {
    "Medical doctors (per 10 000 population)": "med_doctors",
    "UHC SCI components: Infectious diseases": "uhc_infect",
    "UHC SCI components: Noncommunicable diseases": "uhc_noncom",
    "Number of deaths attributed to non-communicable diseases, by type of disease and sex": "deaths_noncom",
    "Life expectancy at birth (years)": "life_expect",
    "Estimated population-based prevalence of depression": "est_preval_dep",
    "Current health expenditure (CHE) as percentage of gross domestic product (GDP) (%)": "health_per_gdp",
    "Prevalence of non-elevated blood pressure (%)": "prev_bp",
    "Prevalence of overweight and obesity": "prev_over",
    "Government health budget (USD)": "health_budg",
    "Income level": "income_levl",
}

header = start_header + [replacements.get(val) for val in header]

# with open("whocsv.csv", "w", newline="") as f :
# 	whocsv_writer = csv.writer(f)
# 	whocsv_writer.writerow(header)
# 	whocsv_writer.writerows(rowslist)


countries = set()


def parser_helper(json_list, values_list):
    json_list = json_list[1]
    #    print(json_list[0]['indicator']['value'])
    for country in json_list[47:]:
        #       print(str(country['country']['value']) + ": " + str(country['value']))
        countries.add(str(country["country"]["value"]))
        values_list.append(country["value"])
    return np.array(values_list)


response = requests.get(
    "https://api.worldbank.org/v2/country/all?format=json&per_page=304"
)
country_list = response.json()[1]
country_list = sorted(country_list, key=lambda k: k["name"])


# country_dict = {}
# for country in country_list:
#    if country['name'] in countries:
#        print(country['id'])
#        print(country['name'] + ": " + str(country['capitalCity']))


# population
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.POP.TOTL?format=json&per_page=15840&date=2018"
)
pop = parser_helper(response.json(), [])


# GDP
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/NY.GDP.MKTP.CD?format=json&per_page=15840&date=2018"
)
gdp = parser_helper(response.json(), [])


## female life expectancy
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.LE00.FE.IN?format=json&per_page=15840&date=2017"
)
fle = parser_helper(response.json(), [])


# male life expectancy
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.LE00.MA.IN?format=json&per_page=15840&date=2017"
)
mle = parser_helper(response.json(), [])


# crude birth rate
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.CBRT.IN?format=json&per_page=15840&date=2017"
)
cbr = parser_helper(response.json(), [])


# fertility rate
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.TFRT.IN?format=json&per_page=15840&date=2017"
)
fert = parser_helper(response.json(), [])


# infant mortality rate
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.IMRT.IN?format=json&per_page=15840&date=2018"
)
imr = parser_helper(response.json(), [])


# communicable disease
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SH.DTH.COMM.ZS?format=json&per_page=15840&date=2016"
)
com = parser_helper(response.json(), [])


# non-communicable disease
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SH.DTH.NCOM.ZS?format=json&per_page=15840&date=2016"
)
noncom = parser_helper(response.json(), [])


##OLD
# for data in data_list:
#    print(str(data))

md_dict = {}
infect_disease_dict = {}
health_exp_dict = {}
# bp_dict = {}
# obesity_dict = {}
health_budget_dict = {}
income_level_dict = {}


##NEW
country_data_list = []
prev = ""
for data in data_list:
    if prev != data.name:
        country_data_list.append(data)
        prev = data.name

for data in country_data_list:
    if data.type == "Medical doctors (per 10 000 population)":
        md_dict[data.fn] = data.val
    elif data.type == "UHC SCI components: Infectious diseases":
        infect_disease_dict[data.fn] = data.val
    elif (
        data.type
        == "Current health expenditure (CHE) as percentage of gross domestic product (GDP) (%)"
    ):
        health_exp_dict[data.fn] = data.val
    #    elif data.type == 'Prevalence of non-elevated blood pressure (%)':
    #        bp_dict[data.fn] = data.val
    #    elif data.type == 'Prevalence of overweight and obesity':
    #        obesity_dict[data.fn] = data.val
    #    elif data.type == 'Government health budget (USD)':
    #        health_budget_dict[data.fn] = data.val
    elif data.type == "Income level":
        income_level_dict[data.fn] = data.val

who_country = []
sort = sorted(list(countries))


# handle stupid cases
sort[13] = "Bahamas"
sort[24] = "Bolivia (Plurinational State of)"
sort[44] = "Democratic Republic of the Congo"
sort[45] = "Congo"
sort[47] = "Côte d'Ivoire"
sort[52] = "Czechia"
sort[58] = "Egypt"
sort[71] = "Gambia"
sort[91] = "Iran (Islamic Republic of)"
sort[103] = "Democratic People's Republic of Korea"
sort[104] = "Republic of Korea"
sort[107] = "Kyrgyzstan"
sort[108] = "Lao People's Democratic Republic"
sort[128] = "Micronesia (Federated States of)"
sort[129] = "Republic of Moldova"
sort[145] = "Republic of North Macedonia"
sort[173] = "Slovakia"
sort[181] = "Saint Kitts and Nevis"
sort[182] = "Saint Lucia"
sort[184] = "Saint Vincent and the Grenadines"
sort[191] = "United Republic of Tanzania"
sort[205] = "United Kingdom of Great Britain and Northern Ireland"
sort[206] = "United States of America"
sort[210] = "Venezuela (Bolivarian Republic of)"
sort[211] = "Viet Nam"
sort[214] = "Yemen"

for data in list(md_dict.keys()):
    if data in sort:
        who_country.append(data)
    else:
        who_country.append("null")

master_country = []

for country in sort:
    if country in who_country:
        master_country.append(country)
    else:
        master_country.append("null")

md = []
infect_disease = []
health_exp = []
# bp = []
# obesity = []
health_budget = []
income_level = []

for item in master_country:
    if item == "null":
        md.append(-999999)
        infect_disease.append(-999999)
        health_exp.append(-999999)
        #      bp.append(-999999)
        #      obesity.append(-999999)
        #       health_budget.append(-999999)
        income_level.append(-999999)
    else:
        md.append(md_dict[item])
        if item in infect_disease_dict:
            infect_disease.append(infect_disease_dict[item])
        else:
            infect_disease.append(-999999)
        if item in health_exp_dict:
            health_exp.append(health_exp_dict[item])
        else:
            health_exp.append(-999999)
        #       bp.append(bp_dict[item])
        #       obesity.append(obesity_dict[item])
        #        if item in health_budget_dict:
        #            health_budget.append(health_budget_dict[item])
        #        else:
        #            health_budget.append(-999999)
        if item in income_level_dict:
            income_level.append(income_level_dict[item])
        else:
            income_level.append(-999999)

test_dict = {
    "name": sort,
    "pop": pop,
    "gdp": gdp,
    "mle": mle,
    "fle": fle,
    "cbr": cbr,
    "imr": imr,
    "com": com,
    "noncom": noncom,
    "md": md,
    "inf_d": infect_disease,
    "hexp": health_exp,
    "incomelevel": income_level,
}
df = pd.DataFrame.from_dict(test_dict)
# df.to_csv("country_info.csv", index = False, sep=',')

wb_dict = {
    "name": sort,
    "pop": pop,
    "gdp": gdp,
    "mle": mle,
    "fle": fle,
    "cbr": cbr,
    "imr": imr,
    "com": com,
    "noncom": noncom,
}
wb_df = pd.DataFrame.from_dict(wb_dict)

if __name__ == "__main__":
    build_disease_list()
