from django.contrib import admin
from germapis.models import Disease

# Register your models here.
admin.site.register(Disease)
