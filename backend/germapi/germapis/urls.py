from rest_framework import routers
from django.urls import include, path

from . import views

router = routers.DefaultRouter()
router.register(r"diseases", views.DiseaseViewset)
router.register(r"years", views.YearViewset)
router.register(r"countries", views.CountryViewset)

urlpatterns = [
    path("", include(router.urls)),
    # path('disease/<str:pk>/', views.DiseaseDetail.as_view())
    # path('disease/', views.DiseaseDetail.as_view())
]
