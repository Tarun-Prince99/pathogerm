#!/usr/bin/env python
# coding: utf-8

# In[27]:


import requests
import json
import pandas as pd
import numpy as np

# In[28]:


countries = set()


def parser_helper(json_list, values_list):
    json_list = json_list[1]
    #    print(json_list[0]['indicator']['value'])
    for country in json_list[47:]:
        #       print(str(country['country']['value']) + ": " + str(country['value']))
        countries.add(str(country["country"]["value"]))
        values_list.append(country["value"])
    return np.array(values_list)


# In[21]:


response = requests.get(
    "https://api.worldbank.org/v2/country/all?format=json&per_page=304"
)
country_list = response.json()[1]
country_list = sorted(country_list, key=lambda k: k["name"])


# country_dict = {}
# for country in country_list:
#    if country['name'] in countries:
#        print(country['id'])
#        print(country['name'] + ": " + str(country['capitalCity']))


# In[45]:


# population
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.POP.TOTL?format=json&per_page=15840&date=2018"
)
pop = parser_helper(response.json(), [])


# In[44]:


# GDP
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/NY.GDP.MKTP.CD?format=json&per_page=15840&date=2018"
)
gdp = parser_helper(response.json(), [])


# In[43]:


## female life expectancy
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.LE00.FE.IN?format=json&per_page=15840&date=2017"
)
fle = parser_helper(response.json(), [])


# In[42]:


# male life expectancy
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.LE00.MA.IN?format=json&per_page=15840&date=2017"
)
mle = parser_helper(response.json(), [])


# In[41]:


# crude birth rate
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.CBRT.IN?format=json&per_page=15840&date=2017"
)
cbr = parser_helper(response.json(), [])


# In[40]:


# fertility rate
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.TFRT.IN?format=json&per_page=15840&date=2017"
)
fert = parser_helper(response.json(), [])


# In[39]:


# infant mortality rate
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SP.DYN.IMRT.IN?format=json&per_page=15840&date=2018"
)
imr = parser_helper(response.json(), [])


# In[38]:


# communicable disease
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SH.DTH.COMM.ZS?format=json&per_page=15840&date=2016"
)
com = parser_helper(response.json(), [])


# In[37]:


# non-communicable disease
response = requests.get(
    "https://api.worldbank.org/v2/country/all/indicator/SH.DTH.NCOM.ZS?format=json&per_page=15840&date=2016"
)
noncom = parser_helper(response.json(), [])


# In[67]:

##OLD
# for data in data_list:
#    print(str(data))

md_dict = {}
infect_disease_dict = {}
health_exp_dict = {}
# bp_dict = {}
# obesity_dict = {}
health_budget_dict = {}
income_level_dict = {}


##NEW
country_data_list = []
prev = ""
for data in data_list:
    if prev != data.name:
        country_data_list.append(data)
        prev = data.name

for data in country_data_list:
    if data.type == "Medical doctors (per 10 000 population)":
        md_dict[data.fn] = data.val
    elif data.type == "UHC SCI components: Infectious diseases":
        infect_disease_dict[data.fn] = data.val
    elif (
        data.type
        == "Current health expenditure (CHE) as percentage of gross domestic product (GDP) (%)"
    ):
        health_exp_dict[data.fn] = data.val
    #    elif data.type == 'Prevalence of non-elevated blood pressure (%)':
    #        bp_dict[data.fn] = data.val
    #    elif data.type == 'Prevalence of overweight and obesity':
    #        obesity_dict[data.fn] = data.val
    #    elif data.type == 'Government health budget (USD)':
    #        health_budget_dict[data.fn] = data.val
    elif data.type == "Income level":
        income_level_dict[data.fn] = data.val

who_country = []
sort = sorted(list(countries))


# handle stupid cases
sort[13] = "Bahamas"
sort[24] = "Bolivia (Plurinational State of)"
sort[44] = "Democratic Republic of the Congo"
sort[45] = "Congo"
sort[47] = "Côte d'Ivoire"
sort[52] = "Czechia"
sort[58] = "Egypt"
sort[71] = "Gambia"
sort[91] = "Iran (Islamic Republic of)"
sort[103] = "Democratic People's Republic of Korea"
sort[104] = "Republic of Korea"
sort[107] = "Kyrgyzstan"
sort[108] = "Lao People's Democratic Republic"
sort[128] = "Micronesia (Federated States of)"
sort[129] = "Republic of Moldova"
sort[145] = "Republic of North Macedonia"
sort[173] = "Slovakia"
sort[181] = "Saint Kitts and Nevis"
sort[182] = "Saint Lucia"
sort[184] = "Saint Vincent and the Grenadines"
sort[191] = "United Republic of Tanzania"
sort[205] = "United Kingdom of Great Britain and Northern Ireland"
sort[206] = "United States of America"
sort[210] = "Venezuela (Bolivarian Republic of)"
sort[211] = "Viet Nam"
sort[214] = "Yemen"

for data in list(md_dict.keys()):
    if data in sort:
        who_country.append(data)
    else:
        who_country.append("null")

master_country = []

for country in sort:
    if country in who_country:
        master_country.append(country)
    else:
        master_country.append("null")

md = []
infect_disease = []
health_exp = []
# bp = []
# obesity = []
health_budget = []
income_level = []

for item in master_country:
    if item == "null":
        md.append(-999999)
        infect_disease.append(-999999)
        health_exp.append(-999999)
        #      bp.append(-999999)
        #      obesity.append(-999999)
        #       health_budget.append(-999999)
        income_level.append(-999999)
    else:
        md.append(md_dict[item])
        if item in infect_disease_dict:
            infect_disease.append(infect_disease_dict[item])
        else:
            infect_disease.append(-999999)
        if item in health_exp_dict:
            health_exp.append(health_exp_dict[item])
        else:
            health_exp.append(-999999)
        #       bp.append(bp_dict[item])
        #       obesity.append(obesity_dict[item])
        #        if item in health_budget_dict:
        #            health_budget.append(health_budget_dict[item])
        #        else:
        #            health_budget.append(-999999)
        if item in income_level_dict:
            income_level.append(income_level_dict[item])
        else:
            income_level.append(-999999)

test_dict = {
    "name": sort,
    "pop": pop,
    "gdp": gdp,
    "mle": mle,
    "fle": fle,
    "cbr": cbr,
    "imr": imr,
    "com": com,
    "noncom": noncom,
    "md": md,
    "inf_d": infect_disease,
    "hexp": health_exp,
    "incomelevel": income_level,
}
df = pd.DataFrame.from_dict(test_dict)
df.to_csv("country_info.csv", index=False, sep=",")
