from django.test import TestCase
from django.http import HttpResponse, JsonResponse, Http404
import unittest
import pandas as pd
import csv
import sys
import requests
from .gbdscraper import (
    build_disease_list,
    md_dict,
    infect_disease_dict,
    health_exp_dict,
    income_level_dict,
    master_country,
    sort,
)
from .gbdscraper import md, infect_disease, health_exp, income_level, wb_df
from rest_framework.test import APIClient, APITestCase, URLPatternsTestCase
from django.urls import include, path, reverse


class TestScraper(unittest.TestCase):
    def test_country_model(self):
        # Check Afghanistan cases
        case_country = "Afghanistan"
        self.assertEqual(md_dict[case_country], "1.899")
        self.assertEqual(infect_disease_dict[case_country], "32")
        self.assertEqual(income_level_dict[case_country], "Low")

    def test_failed_country_model(self):
        # Check cases where no data is present are being filled in
        bad_indexes = []
        country_index = 0
        for country in master_country:
            if country == "null":
                bad_indexes.append(country_index)
            country_index += 1

        for index in bad_indexes:
            self.assertEqual(md[index], -999999)
            self.assertEqual(infect_disease[index], -999999)
            self.assertEqual(health_exp[index], -999999)
            self.assertEqual(income_level[index], -999999)

    def test_world_bank_scraper(self):
        # check information from the world bank api
        self.assertEqual(wb_df.iloc[4, 6], 2.7)
        self.assertEqual(wb_df.iloc[5, 6], 51.6)
        self.assertEqual(wb_df.iloc[6, 1], 96286)
        self.assertEqual(wb_df.iloc[30, 4], 61.43)
        self.assertEqual(wb_df.iloc[100, 3], 68.72)
        self.assertEqual(wb_df.iloc[5, 7], 63.4)
        self.assertEqual(wb_df.iloc[163, 8], 81)
        self.assertEqual(wb_df.iloc[120, 5], 16.867)

    def test_api_response(self):
        self.assertEqual(requests.get("http://api.pathogerm.com/").status_code, 200)
        self.assertEqual(
            requests.get("http://api.pathogerm.com/diseases").status_code, 200
        )
        self.assertEqual(
            requests.get("http://api.pathogerm.com/years").status_code, 200
        )
        self.assertEqual(
            requests.get("http://api.pathogerm.com/countries").status_code, 200
        )

    def test_fake_api_response(self):
        self.assertEqual(requests.get("http://api.pathogerm.com/foo").status_code, 404)
        self.assertEqual(requests.get("http://api.pathogerm.com/bar").status_code, 404)
        self.assertEqual(
            requests.get("http://api.pathogerm.com/years/1600").status_code, 404
        )
        self.assertEqual(
            requests.get("http://api.pathogerm.com/countries/Michael").status_code, 404
        )


class TestDisease(APITestCase):
	def test_diseases(self):
		self.client = APIClient()
		response = self.client.get("/diseases/", format='json')
		self.assertEqual(response.status_code, 200)

	def test_countries(self):
		self.client = APIClient()
		response = self.client.get("/countries/", format='json')
		self.assertEqual(response.status_code, 200)

	def test_years(self):
		self.client = APIClient()
		response = self.client.get("/years/", format='json')
		self.assertEqual(response.status_code, 200)

	def test_invalid(self):
		self.client = APIClient()
		response = self.client.get("/fake", format = 'json')
		self.assertEqual(response.status_code, 404)

	def test_disease_content(self):
		self.client = APIClient()
		response = self.client.get("/countries/?iso=USA&limit=1", format = 'json')
		data = {"count":0,"next":None,"previous":None,"results":[]}
		self.assertEqual(response.data, data)

	def bad_test_disease_content(self):
		self.client = APIClient()
		response = self.client.get("/countries/?iso=FAKE&limit=1", format = 'json')
		data = {"count":0,"next":None,"previous":None,"results":[]}
		self.assertEqual(response.data, data)

class TestSearch(APITestCase):
    def test_disease_search(self):
        response = requests.get("https://api.pathogerm.com/diseases/?cause=Acne%20vulgaris&location=AUS&year=1999&measure=Incidence")
        data =  {'count': 1, 
                'next': None, 
                'previous': None, 
                'results': [{'cause': 'Acne vulgaris', 
                            'measure': 'Incidence', 
                            'year': 1999, 
                            'location': 'AUS', 
                            'description': 'https://en.wikipedia.org/api/rest_v1/page/summary/Acne', 
                            'number': 275957.91188, 
                            'percent': 0.00335088075327, 
                            'rate': 1478.30290473}]}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), data)

    def test_disease_client(self):
        self.client = APIClient()
        response = self.client.get("/diseases/?search=acute")
        self.assertEqual(response.status_code, 200)
    
    def test_country_search(self):
        response = requests.get("https://api.pathogerm.com/countries/?search=usa")
        data = {'count': 1,
                'next': None,
                'previous': None,
                'results': [{'iso': 'USA',
                'name': 'United States of America',
                'pop': 327167434.0,
                'gdp': 20544343456936.5,
                'mle': 76.1,
                'fle': 81.1,
                'cbr': 11.8,
                'imr': 5.6,
                'com': 5.2,
                'noncom': 88.3,
                'md': 25.948,
                'inf_d': 79.0,
                'hexp': 12.5,
                'incomelevel': 'High'}]}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), data)

    def test_country_matching(self):
        response = requests.get("https://api.pathogerm.com/countries?search=america")
        # should also match american samoa
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 2)

    def test_country_client(self):
        self.client = APIClient()
        response = self.client.get("/countries/?search=usa", format='json')
        self.assertEqual(response.status_code, 200)

    def test_year_search(self):
        response = requests.get("https://api.pathogerm.com/years/?year=2017")
        self.assertEqual(response.status_code,200)

    def test_year_search_return(self):
        # test filter and search
        response = requests.get("https://api.pathogerm.com/years/?name=USA&search=2001")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["results"][0]['health_per_gdp'], 13.2)

    def test_year_client(self):
        self.client = APIClient()
        response = self.client.get("/years/?year=2015", format='json')
        self.assertEqual(response.status_code, 200)

    def test_invalid(self):
        # invalid ISO category
        response = requests.get("https://api.pathogerm.com/years/?name=ABCD")
        self.assertEqual(response.status_code, 400)

    def test_empty(self):
        # empty search
        response = requests.get("https://api.pathogerm.com/years/?year=1492")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['count'], 0)

if __name__ == "__main__":
    unittest.main()
