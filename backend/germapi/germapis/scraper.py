import json
import requests
import csv
from pprint import pprint


class Data_Entry:
    def __init__(self, name, full_name, value, type, year):
        self.name = name
        self.fn = full_name
        self.val = value
        self.type = type
        self.year = year

    def __str__(self):
        return (
            self.fn
            + "("
            + self.name
            + "): "
            + self.val
            + " of label "
            + self.type
            + " in "
            + self.year
        )

    def name(self):
        return self.fn

    def type(self):
        return self.type

    def __iter__(self):
        yield self.name
        yield self.fn
        yield self.val
        yield self.type
        yield self.year


urltemplate = "http://apps.who.int/gho/athena/api/GHO{mylabel}/?format=json"
url = urltemplate.format(mylabel="")
# data = urllib.request.urlopen(url)

data = requests.get(url).json()

# pprint(data['dimension'])
dim = data["dimension"][0]
code_list = dim["code"]

country_list = {}
label_list = []
desc_list = []
i = 0
# obtain list of labels
for val in code_list:
    label_list.append(val["label"])
    desc_list.append(val["display"])

final_label_list = {}
final_label_list[label_list[3191]] = desc_list[3191]
final_label_list[label_list[3277]] = desc_list[3277]
final_label_list[label_list[3278]] = desc_list[3278]
final_label_list[label_list[2770]] = desc_list[2770]
final_label_list[label_list[56]] = desc_list[56]
final_label_list[label_list[2765]] = desc_list[2765]
final_label_list[label_list[2733]] = desc_list[2733]
final_label_list[label_list[2487]] = desc_list[2487]  # No entries
final_label_list[label_list[2160]] = desc_list[2160]  # No entries
final_label_list[label_list[255]] = desc_list[255]
final_label_list[label_list[428]] = desc_list[428]

# for key in final_label_list:
# 	print(key,' : ', final_label_list[key])

# pprint(page_data['fact'][5])
country_url = "http://apps.who.int/gho/athena/api/COUNTRY?format=json"
country_page = requests.get(country_url).json()

# populate country list with key-value pairing code with country name
for items in country_page["dimension"][0]["code"]:
    country_list[items["label"]] = items["display"]
country_list["NOT FOUND"] = "NOT FOUND"

data_list = []
# print(final_label_list.keys())
for keys in final_label_list.keys():
    urlend = "/" + keys
    newurl = urltemplate.format(mylabel=urlend)
    page_data = requests.get(newurl).json()
    # 	pprint(len(page_data['fact']))
    for item in page_data["fact"]:
        name = "NOT FOUND"
        year = -1
        for subitem in item["Dim"]:
            if subitem["category"] == "COUNTRY":
                name = subitem["code"]
            elif subitem["category"] == "YEAR":
                year = subitem["code"]
        # print('name: ', name, ', year: ', year, 'val: ', item['value']['display'], ', type: ', final_label_list[keys])
        data_list.append(
            Data_Entry(
                name,
                country_list[name],
                item["value"]["display"],
                final_label_list[keys],
                year,
            )
        )

rows = {}
start_header = ["name", "full_name", "year"]
header = []
# number of attributes in list is hardcoded to 11
for entry in data_list:
    key = (entry.name, entry.fn, entry.year)
    if key not in rows:
        rows[key] = [None] * 9
    if entry.type not in header:
        header.append(entry.type)
    rows[key][header.index(entry.type)] = entry.val

rowslist = []
for key in rows:
    rowslist.append(list(key) + rows.get(key))

replacements = {
    "Medical doctors (per 10 000 population)": "med_doctors",
    "UHC SCI components: Infectious diseases": "uhc_infect",
    "UHC SCI components: Noncommunicable diseases": "uhc_noncom",
    "Number of deaths attributed to non-communicable diseases, by type of disease and sex": "deaths_noncom",
    "Life expectancy at birth (years)": "life_expect",
    "Estimated population-based prevalence of depression": "est_preval_dep",
    "Current health expenditure (CHE) as percentage of gross domestic product (GDP) (%)": "health_per_gdp",
    "Prevalence of non-elevated blood pressure (%)": "prev_bp",
    "Prevalence of overweight and obesity": "prev_over",
    "Government health budget (USD)": "health_budg",
    "Income level": "income_levl",
}

header = start_header + [replacements.get(val) for val in header]

# with open("whocsv.csv", "w", newline="") as f :
# 	whocsv_writer = csv.writer(f)
# 	whocsv_writer.writerow(header)
# 	whocsv_writer.writerows(rowslist)
