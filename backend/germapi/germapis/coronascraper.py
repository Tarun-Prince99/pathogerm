import csv


class CountryInformation:
    def __init__(self, date, country, newCases, newDeaths, totalCases, totalDeaths):
        self.date = date
        self.country = country
        self.nc = newCases
        self.nd = newDeaths
        self.tc = totalCases
        self.td = totalDeaths

    def __str__(self):
        return (
            self.country
            + " on "
            + self.date
            + ": "
            + self.nc
            + " , "
            + self.nd
            + " , "
            + self.tc
            + " , "
            + self.td
        )


countryList = []
with open("full_data.csv", "r") as file:
    reader = csv.reader(file)
    for row in reader:
        # print(str(row[1]) + str(row[2]) + str(row[3]) + str(row[4]) + str(row[5]))
        countryList.append(
            CountryInformation(
                str(row[0]),
                str(row[1]),
                str(row[2]),
                str(row[3]),
                str(row[4]),
                str(row[5]),
            )
        )

for country in countryList[1:]:
    print(str(country))
