#!/usr/bin/env python
import psycopg2

try:
    connection = psycopg2.connect(
        user = 'SECRET',
        password = 'SECRET',
        host = 'SECRET',
        database = 'SECRET'
    )
    cursor = connection.cursor()
    print("Database connection established")

    # Print PostgreSQL version
    cursor.execute("SELECT version();")
    record = cursor.fetchone()
    print("You are connected to - ", record,"\n")
    cursor.execute(
        "SELECT DISTINCT cause FROM germapis_disease"
    )
    rows = cursor.fetchall()
    for row in rows :
        # Get old description
        connString = "SELECT DISTINCT description FROM germapis_disease WHERE cause='{}'".format(row[0].replace("'", "''"))
        print(connString)
        cursor.execute(connString)
        oldDesc = cursor.fetchone()
        print(oldDesc)
        # Format to new description and apply to database
        newDesc = oldDesc[0].replace("wiki/", "api/rest_v1/page/summary/")
        print(newDesc)
        connString = "UPDATE germapis_disease SET description='{}' WHERE cause='{}'".format(newDesc, row[0].replace("'", "''"))
        print(connString)
        cursor.execute(connString)
    print("STARTING COMMIT")
    connection.commit()
    print("FINISHED COMMIT")
except Exception as e:
    print(e)
finally:
    if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
